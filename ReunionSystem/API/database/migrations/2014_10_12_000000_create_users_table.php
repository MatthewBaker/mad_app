<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('login_id');
            $table->string('email')->unique();
            $table->string('contact_number');
            //$table->string('first_name');
            $table->string('password', 60);
            $table->string('default_language');
            $table->string('nationality');
            $table->timestamp('last_login');
            $table->enum('status', ['active', 'inactive', 'suspended']);
            //$table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
