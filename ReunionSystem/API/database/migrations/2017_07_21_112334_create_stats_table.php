<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('store_id');
            $table->integer('question_id');
            $table->integer('year');
            $table->integer('month');
            $table->string('visit_1_answer');
            $table->integer('visit_1_points');
            $table->text('visit_1_comment');
            $table->text('visit_1_image');
            $table->timestamp('visit_1_device_time');
            $table->timestamp('visit_1_server_time');
            $table->text('visit_2_answer');
            $table->integer('visit_2_points');
            $table->text('visit_2_comment');
            $table->text('visit_2_image');
            $table->timestamp('visit_2_device_time');
            $table->timestamp('visit_2_server_time');
            $table->text('visit_3_answer');
            $table->integer('visit_3_points');
            $table->text('visit_3_comment');
            $table->text('visit_3_image');
            $table->timestamp('visit_3_device_time');
            $table->timestamp('visit_3_server_time');
            $table->text('visit_4_answer');
            $table->integer('visit_4_points');
            $table->text('visit_4_comment');
            $table->text('visit_4_image');
            $table->timestamp('visit_4_device_time');
            $table->timestamp('visit_4_server_time');
            $table->tinyInteger('completed');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stats');
    }
}
