<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('status');
            $table->string('trade_category');
            $table->string('descripton');
            $table->bigInteger('territory_id');
            $table->string('isms_code');
            $table->string('profile');
            $table->string('latitude');
            $table->string('longititude');
            $table->string('province');
            $table->string('city');
            $table->string('suburb');
            $table->string('street');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
