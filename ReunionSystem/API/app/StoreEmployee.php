<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreEmployee extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $table = 'store_employees';
    protected $dates = ['deleted_at'];
    protected $fillable = [
    'store_id',
    'area_manager_1_name',
    'area_manager_1_tel',
	'area_manager_2_name',
	'area_manager_2_tel',
	'pos_address',
	'cashier_1_name',
	'cashier_1_tel',
	'cashier_1_mail',
	'cashier_1_date_of_birth',
	'cashier_2_name',
	'cashier_2_tel',
	'cashier_2_mail',
	'cashier_2_date_of_birth',
	'cashier_3_name',
	'cashier_3_tel',
	'cashier_3_mail',
	'cashier_3_date_of_birth'
	];
}

