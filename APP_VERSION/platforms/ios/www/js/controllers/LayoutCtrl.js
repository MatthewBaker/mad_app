/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *  LayoutCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('LayoutCtrl', function($ionicPlatform, $scope, $state, LoaderService, $ionicPopup, ipCookie, $rootScope, dataService){
            $ionicPlatform.ready(function() {
                $scope.layout = {
                    breadcrumbs : [],
                    store :{
                        name : '',
                        id : '',
                        month : '',
                        current_stats : '',
                        past_stats : '',
                        call_status : '',
                        position : ''
                    },
                    user : '',
                    world : '',
                    call_completed : false
                };

                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.layout.user =  ipCookie('LOGIN', undefined, {decode: function (value) { return value; }});
                    $scope.netstate = checknet;
                    $scope.app_version = app_version;
                });

                if(window.Connection){
                    // listen for Online event
                    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                        $scope.netstate = 'online';
                        LoaderService.showLoading('Synching data');
                        dataService.sync().success(function (result) {
                            if(result != 'active'){
                                ipCookie.remove('LOGIN');
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                                    template:'<div class="popup-subtitle">Incorrect login details</div>',
                                    buttons: [
                                        {
                                            text: 'To login page',
                                            onTap: function () {
                                                $state.go('login');
                                            }
                                        }
                                    ]
                                });
                            }
                            LoaderService.hideLoading();
                        }).error(function(){
                            ipCookie.remove('LOGIN');
                            LoaderService.hideLoading();
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'To login page',
                                        onTap: function () {
                                            $state.go('login');
                                        }
                                    }
                                ]
                            });
                        });
                    });

                    // listen for Offline event
                    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                        $scope.netstate = 'offline';
                    });
                }

                $scope.crumbGo = function(A){
                    if(A != '')
                        $state.go(A);
                };

                $scope.logout = function(){
                    $ionicPopup.show({
                        title: '<div class="popup-pmi-title">Are you sure you want to sign out?</div>',
                        buttons: [
                            {
                                text: 'Yes',
                                type: 'button-pmi',
                                onTap: function () {
                                    $state.go('login');
                                    ipCookie.remove('LOGIN');
                                    ipCookie.remove('ALL_STORES');
                                    ipCookie.remove('LIST_STORES');
                                    ipCookie.remove('WORLD_SCORE');
                                    ipCookie.remove('SYNCH');
                                }
                            },
                            {
                                text: 'No',
                                type: 'button-pmi',
                                onTap: function () {

                                }
                            }
                        ]
                    });
                }
            });
        })

})();