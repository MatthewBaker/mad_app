/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *  storeSummaryCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('storeSummaryCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService, $ionicPopup){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    LoaderService.showLoading('Loading...');
                    $scope.store = $scope.layout.store;
                    $scope.call_completed = $scope.layout.call_completed;
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Home',link:'app.storeHome'},
                        {title:'Summary',link:''}
                    ];

                    dataService.store().success(function (result) {
                        var resultser = JSON.parse(result);
                        var serviceStats = resultser[$scope.layout.store.id];
                        $scope.currentMonth = 0;
                        $scope.currentYear = 0;
                        $scope.points = 0;
                        $scope.VisistPoints = [0, 0, 0, 0];
                        $scope.current_image_store = [];
                        $scope.page = {};


                        if($scope.layout.store.call_status == 'Continuer'){
                            $scope.page.currentStats = serviceStats['CURRENT_STATS'] ;
                            angular.forEach($scope.page.currentStats,function(value, key) {
                                $scope.currentYear = key;
                            });
                            angular.forEach($scope.page.currentStats[$scope.currentYear],function(value, key) {
                                $scope.currentMonth = key;
                            });
                        }else if ($scope.layout.store.call_status == 'Terminé'){
                            var currentdate = new Date();
                            var month = currentdate.getMonth()+1;
                            var year = currentdate.getFullYear();
                            $scope.page.currentStats = serviceStats['PAST_STATS'] ;
                            $scope.currentMonth  = month;
                            $scope.currentYear  = year;
                        }else{
                            $scope.page.currentStats = serviceStats['PAST_STATS'] ;
                            $scope.currentMonth  = $scope.layout.month;
                            $scope.currentYear = $scope.layout.year;
                        }

                        $scope.DV1 = 1;$scope.DV2 = 1;$scope.DV3 = 1;$scope.DV4 = 1;

                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth])
                            $scope.question_number = Object.keys($scope.page.currentStats[$scope.currentYear][$scope.currentMonth]).length;

                        for (var i = 1; i < $scope.question_number + 1; i++) {
                            if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth] && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i]){
                                var V1P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V1A == 'yes') ? 1 : 0 ;
                                var V2P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V2A == 'yes') ? 1 : 0 ;
                                var V3P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V3A == 'yes') ? 1 : 0 ;
                                var V4P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V4A == 'yes') ? 1 : 0 ;
                                $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i]['POINTS'] = V1P4 + V2P4 + V3P4 + V4P4;
                                $scope.points += V1P4 + V2P4 + V3P4 + V4P4;
                                $scope.VisistPoints[0] += V1P4;
                                $scope.VisistPoints[1] += V2P4;
                                $scope.VisistPoints[2] += V3P4;
                                $scope.VisistPoints[3] += V4P4;
                            }
                        };

                        LoaderService.hideLoading();
                    });
                });

                // BACK TO DETAILS
                $scope.backToHome = function(){
                    $state.go('app.storeHome');
                }

                $scope.goToQ = function(){
                    $state.go('app.storeQuestions');
                }

                $scope.commentModal = function(stats, question, visit){
                    if(stats == 'true')
                    {
                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] !='' && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] != null){
                            $scope.comment_modal = stats;


                            $scope.visit_val = visit;
                            $scope.question_val = question;
                            $scope.commentvalue = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'];
                        }else{
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Warning</div>',
                                template:'<div class="popup-subtitle">No Note made.</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        }
                    }else{
                        $scope.comment_modal = '';
                    }
                }

                $scope.imageModal = function(stats, question, visit){
                    if(stats == 'true')
                    {
                        $scope.current_image_store = [];
                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != '' && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != null){
                            $scope.image_modal = stats;
                            if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != null){
                                $scope.imgSRC = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'];
                                $scope.current_image_store = $scope.imgSRC.split('|');
                            }else{
                                $scope.imgSRC = 'img/placeholder.png';
                            }
                        }else{
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Warning</div>',
                                template:'<div class="popup-subtitle">No Image taken.</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        }
                    }else{
                        $scope.image_modal = '';
                    }
                }
            });
        })

})();