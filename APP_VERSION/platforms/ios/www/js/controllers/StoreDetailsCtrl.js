/**
 * Created by anele on 2017/07/13.
 */


/* =============================================================================
 *  StoreDetailsCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('StoreDetailsCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:''},
                        {title:'Performance Dimensions',link:''}
                    ];
                    $scope.store = $scope.layout.store;

                  

                    // CHECK CURRENT MONTH
                    var dateFunc = new Date();
                    $scope.month = dateFunc.getMonth()+1;

                    angular.forEach($scope.layout.store.current_stats,function(value, key) {
                        $scope.Statmonth = key;
                    });

                    // PAST STATS
                    var pastData = Array();
                    var worldData = '';
                    if($scope.layout.store.past_stats == '' || (Array.isArray($scope.layout.store.past_stats) && $scope.layout.store.past_stats.length < 1) || Object.keys($scope.layout.store.past_stats).length < 1){
                        pastData = ['0','0','0','0','0','0','0','0','0','0','0'];
                        worldData = ['0','0','0','0','0','0','0','0','0','0','0'];
                        $scope.emptyGraph = '0';
                    }else{
                        $scope.emptyGraph = '1';
                        worldData = $scope.layout.world;
                        angular.forEach($scope.layout.store.past_stats,function(yearData, year) {
                            angular.forEach(yearData,function(monthData, month) {
                                angular.forEach(monthData,function(QuestionData, Question) {
                                    var V1P1 = (QuestionData.V1A == 'yes') ? 1 : 0 ;
                                    var V2P1 = (QuestionData.V2A == 'yes') ? 1 : 0 ;
                                    var V3P1 = (QuestionData.V3A == 'yes') ? 1 : 0 ;
                                    var V4P1 = (QuestionData.V4A == 'yes') ? 1 : 0 ;
                                    if(pastData[Question - 1]){
                                        pastData[Question - 1] = parseInt(pastData[Question - 1]) +  V1P1 + V2P1 + V3P1 + V4P1;
                                    }else{
                                        pastData[Question - 1] = V1P1 + V2P1 + V3P1 + V4P1;
                                    }
                                });
                            });
                        });
                    }

                    // TOTAL POINTS
                    $scope.totalPoints = 0;
                    angular.forEach(pastData,function(value, key) {
                        $scope.totalPoints += parseInt(value);
                    });

                    // GRAPH
                    $scope.graph = {};
                    $scope.graph.labels = ['1','2','3','4','5','6','7','8','9','10','11','12','13'];
                    $scope.graph.data = [
                        pastData,
                        worldData
                    ];
                    $scope.graph.colours = [
                        {
                            fillColor: '#38538E'
                        },
                        {
                            fillColor: '#2B4273'
                        }
                    ];
                    $scope.graph.series = ['SA Score', 'World Wide Score'];
                    $scope.graph.options = {
                        scaleGridLineColor: "#fff",
                        scaleShowVerticalLines: false,
                        scaleFontFamily: "'source_sans_proregular', 'Helvetica', 'Arial', sans-serif",
                        tooltipFillColor: "#231f20",
                        tooltipFontFamily: "'source_sans_proregular', 'Helvetica', 'Arial', sans-serif",
                        tooltipTitleFontFamily: "'source_sans_proregular', 'Helvetica', 'Arial', sans-serif",
                        tooltipTitleFontStyle: "normal",
                        scaleFontColor: "#fff",
                        barValueSpacing: 10,
                        barShowStroke: false,
                        barDatasetSpacing: 0,
                        scaleLineColor: "#fff"
                    }
                });

                // CHECK PAST RESULTS
                $scope.pastResults = function(){
                    $state.go('app.pastResults');
                };

                // GO TO HOME
                $scope.goToHome = function(){
                    if($scope.layout.store.call_status == 'Démarrer')
                    {
                        LoaderService.showLoading('Starting Call');
                        dataService.start($scope.layout.store.id).success(function (result) {
                            $scope.layout.store.current_stats = result['CURRENT_STATS'];
                            $scope.layout.store.call_status = result['CALL_STATUS'];
                            $scope.layout.store.position = result['POSITION'];
                            LoaderService.hideLoading();
                            $state.go('app.storeHome');
                        });
                    }else{
                        LoaderService.hideLoading();
                        $state.go('app.storeHome');
                    }
                }
            });
        })

})();