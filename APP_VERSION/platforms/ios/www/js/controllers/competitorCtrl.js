/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *  competitorCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('competitorCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService, $ionicPopup, $ionicModal){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    LoaderService.showLoading('Loading...');
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Home',link:'app.storeHome'},
                        {title:'Competitors',link:''}
                    ];

                    $scope.change = false;
                    $scope.notemodal = {};
                    $scope.notemodal.comment_value = '';
                    $scope.imagemodal = {};
                    $scope.competitor_image_store = {};

                    $scope.store = $scope.layout.store;
                    $scope.competitors = $scope.layout.competitor;
                    $scope.competitor_values = {};
                    $scope.competitor_posting_values = {};

                    angular.forEach($scope.competitors,function(value, key) {
                        $scope.competitor_values[key] = { images: '', comment : ''};
                        $scope.competitor_posting_values[key] = { images: '', comment : ''};
                        $scope.competitor_image_store[key] = [];
                    });

                    $scope.modals = {};
                    $scope.modals.current_competitor_id = '';

                    $ionicModal.fromTemplateUrl('imageModal.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        $scope.imageModal = modal;
                    });
                    $ionicModal.fromTemplateUrl('noteModal.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        $scope.noteModal = modal;
                    });

                    LoaderService.hideLoading();
                });

                // TAKE PICTURE
                $scope.takePicture = function(){
                    LoaderService.showLoading('Taking Picture...');
                    var currentdate = new Date();
                    var datetime =  currentdate.getFullYear() +"-"
                        + (currentdate.getMonth()+1) +"-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    navigator.camera.getPicture(onSuccess, onFail, { quality: 100,targetWidth: 480,targetHeight: 480,
                        destinationType: Camera.DestinationType.DATA_URL
                    });

                    function onSuccess(imageData) {
                        if($scope.competitor_values[$scope.modals.current_competitor_id]['images'] == '')
                            $scope.competitor_values[$scope.modals.current_competitor_id]['images'] = "data:image/jpeg;base64," + imageData;
                        else
                            $scope.competitor_values[$scope.modals.current_competitor_id]['images'] = $scope.competitor_values[$scope.modals.current_competitor_id]['images'] + "|data:image/jpeg;base64," + imageData;

                        $scope.competitor_image_store[$scope.modals.current_competitor_id] = $scope.competitor_values[$scope.modals.current_competitor_id]['images'].split('|');
                        LoaderService.hideLoading();
                    }

                    function onFail(message) {
                        LoaderService.hideLoading();
                        alert('Failed because: ' + message);
                    }
                }

                $scope.backToHome = function(){
                    $state.go('app.storeHome');
                }

                // IMAGE MODAL
                $scope.showImageModal = function(competitor_id){
                    $scope.modals.current_competitor_id = competitor_id;
                    $scope.imageModal.show();
                }

                // COMMENT MODAL
                $scope.showCommentModal = function(competitor_id){
                    $scope.modals.current_competitor_id = competitor_id;
                    $scope.noteModal.show();
                }

                // CLOSE MODAL
                $scope.close_modal = function(type){
                    var modal_type = ( type === 'images' )? 'imageModal' : 'noteModal' ;
                    $scope[modal_type].hide();
                }

                $scope.save_data = function(type, value){
                    if(type == 'comment'){
                        if(value && value != ''){
                            $scope.change = true;
                            $scope.competitor_posting_values[$scope.modals.current_competitor_id]['comment'] = value;
                            $scope.close_modal(type);
                        }
                    }else if (type == 'images'){
                        $scope.change = true;
                        $scope.competitor_posting_values[$scope.modals.current_competitor_id]['images'] = value;
                        $scope.close_modal(type);
                    }
                }

                // UPDATE COMPETITOR
                $scope.update_competitors = function(type){
                    $scope.close_modal(type);
                    LoaderService.showLoading('Saving data...');
                    dataService.updateComp($scope.competitor_posting_values, $scope.layout.store.id).success(function (result) {
                        LoaderService.hideLoading();
                        $scope.change = false;
                    }).error(function(){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Update Competitors Faileds</div>',
                            template:'<div class="popup-subtitle">Data Error</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    });
                }
            });
        })

})();