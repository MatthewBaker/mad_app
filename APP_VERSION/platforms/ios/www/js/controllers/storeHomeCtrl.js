/**
 * Created by anele on 2017/07/13.
 */


/* =============================================================================
 *  storeHomeCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('storeHomeCtrl', function($ionicPlatform, $ionicPopup,$scope, $state, LoaderService, dataService, $cordovaGeolocation){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.store = $scope.layout.store;
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Home',link:''}
                    ];
                });

                // GRAB LOCATION
                $scope.getLocation = function(){
                    var posOptions = {timeout: 10000, enableHighAccuracy: false};
                    $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position){
                        $scope.layout.store.position.LAT = position.coords.latitude;
                        $scope.layout.store.position.LONG = position.coords.longitude;

                        dataService.updatePos($scope.layout.store.position.LAT , $scope.layout.store.position.LONG,$scope.layout.store.id).success(function (result) {
                            if(checknet == 'online'){
                                dataService.sync().success(function (result) {
                                    LoaderService.hideLoading();
                                    if(result != 'active'){
                                        ipCookie.remove('LOGIN');
                                        $ionicPopup.show({
                                            title: '<div class="popup-pmi-title">Update Questions Synching</div>',
                                            template:'<div class="popup-subtitle">Incorrect login details</div>',
                                            buttons: [
                                                {
                                                    text: 'To login page',
                                                    onTap: function () {
                                                        $state.go('login');
                                                    }
                                                }
                                            ]
                                        });
                                    }
                                }).error(function(){
                                    LoaderService.hideLoading();
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Update questions Sync Failed</div>',
                                        template:'<div class="popup-subtitle">Data Error</div>',
                                        buttons: [
                                            {
                                                text: 'To login page',
                                                onTap: function () {
                                                    $state.go('login');
                                                }
                                            }
                                        ]
                                    });
                                });
                            }else{
                                LoaderService.hideLoading();
                            }
                        }).error(function(){
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Update Location Failed</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        });
                    }, function(error){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Error</div>',
                            template:'<div class="popup-subtitle">Location Cannot be captured.</div>',
                            buttons: [
                                {
                                    text: 'Ok'
                                }
                            ]
                        });
                    });
                };

                // BACK TO DETAILS
                $scope.backToDetails = function(){
                    $state.go('app.storeDetails');
                };

                // REDIRECT
                $scope.goTo = function(A){
                    LoaderService.showLoading('Loading...');
                    if($scope.layout.store.call_status == "Démarrer")
                    {
                        LoaderService.showLoading('Starting Call');
                        dataService.start($scope.layout.store.id).success(function (result) {
                            $scope.layout.store.current_stats = result['CURRENT_STATS'];
                            $scope.layout.store.call_status = result['CALL_STATUS'];
                            $scope.layout.store.position = result['POSITION'];
                            LoaderService.hideLoading();
                            $state.go(A);
                        });
                    }else{
                        $state.go(A);
                    }
                }
            });
        })

})();