// Ionic Starter App


// GLOBAL VARAIBLES
  var db = null;
  var app_version = '3.0.0';
  //var url = 'https://pmi-reunion-app.optimalonline.co.za/application';
  //var url = 'https://pmi-reunion-app-2.optimalonline.co.za/application';
  var url = 'http://localhost:3000/application';
  var checknet = 'online';

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter',
    [
        'ionic',
        'ngCordova',
        'chart.js',
        'ipCookie',
        'ngAnimate',
        'oc.lazyLoad',
        'ui.bootstrap'
    ]);

// PAGE CONTROLLERS


// FILTERS
    // SEARCH FILTER FOR OUTLETS
        app.filter('StoresFilter',function($filter){
           return function(items, text){
              if (!text || text.length === 0)
                return items;
              
              // split search text on space
              var searchTerms = text.split(' ');
              
              // search for single terms.
              // this reduces the item list step by step
              searchTerms.forEach(function(term) {
                if (term && term.length)
                  items = $filter('filter')(items, term);
              });

              return items
           };
        });