/**
 * Created by anele on 2017/07/13.
 */


/*
 *	Handle our routes and module deps for those pages
 * =============================================================================
 */

(function(){
    'use strict';

    angular
    .module('starter')
    .run(function ($rootScope,   $state,   $stateParams) {
        $rootScope.$state 				= $state;
        $rootScope.$stateParams 	= $stateParams;
    })

    .config(function Config ( $stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $ionicConfigProvider ) {

        $urlRouterProvider.otherwise("/config");

        $ionicConfigProvider.views.swipeBackEnabled(false);

        $stateProvider

        // CONFIG
        .state('config', {
            url: "/config",
            templateUrl: "views/config.html",
            controller: 'ConfigCtrl',
            resolve : { }
        })

        // LOGIN
        .state('login', {
            url: "/login",
            templateUrl: "views/login.html",
            controller: 'LoginCtrl'
        })

        // LAYOUT
        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "views/layout.html",
            controller: 'LayoutCtrl'
            /*resolve : {
                deps : ['$ocLazyLoad', function( $ocLazyLoad ){
                    return $ocLazyLoad.load(['js/controllers/LayoutCtrl.js'])
                }]
            }*/
        })

        // STORES
        .state('app.stores', {
            url: "/stores",
            views: {
                'app': {
                    templateUrl: "views/stores.html",
                    controller: 'StoresCtrl',
                    /*resolve : {
                        deps :['$ocLazyLoad', function( $ocLazyLoad ){
                            return $ocLazyLoad.load(['js/controllers/StoresCtrl.js']);
                        }]
                    }*/
                }
            }
        })

        // STORE DETAILS
        .state('app.storeDetails', {
            url: "/storedetails",
            views: {
                'app': {
                    templateUrl: "views/storeDetails.html",
                    controller: 'StoreDetailsCtrl'
                }
            }
        })

        // PAST RESULTS
        .state('app.pastResults', {
            url: "/pastresults",
            views: {
                'app': {
                    templateUrl: "views/pastResults.html",
                    controller: 'pastResultsCtrl'
                }
            }
        })

        // STORE HOME
        .state('app.storeHome', {
            url: "/storehome",
            views: {
                'app': {
                    templateUrl: "views/storeHome.html",
                    controller: 'storeHomeCtrl'
                }
            }
        })

        // STORE DATA COLLECTION
        .state('app.data-collection', {
            url: "/data-collection",
            views: {
                'app': {
                    templateUrl: "views/data-collection.html",
                    controller: 'dataCollectionCtrl'
                }
            }
        })

        // STORE QUESTIONS
        .state('app.storeQuestions', {
            url: "/storequestions",
            views: {
                'app': {
                    templateUrl: "views/storeQuestions.html",
                    controller: 'storeQuestionsCtrl'
                }
            }
        })

        // STORE SUMMARY
        .state('app.storeSummary', {
            url: "/storesummary",
            views: {
                'app': {
                    templateUrl: "views/storeSummary.html",
                    controller: 'storeSummaryCtrl'
                }
            }
        })

        // STORE QUESTIONS
        .state('app.competitor', {
            url: "/competitors",
            views: {
                'app': {
                    templateUrl: "views/competitor.html",
                    controller: 'competitorCtrl'
                }
            }
        })
    })
})();