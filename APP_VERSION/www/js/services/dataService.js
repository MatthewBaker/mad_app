/*jshint sub:true*/
/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *
 * =============================================================================
 */
function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
    return JSON.stringify(obj) === JSON.stringify({});
}

var app =
    angular
    .module('starter')

    .service('RequestService', function($http) {
        return {
            post: function(type,postData) {
                var currentdate = new Date();
                
                var datetime =  currentdate.getFullYear() +"-" + 
                    (currentdate.getMonth()+1) +"-"+ 
                    currentdate.getDate() + " "+ 
                    currentdate.getHours() + ":" + 
                    currentdate.getMinutes() + ":"+ 
                    currentdate.getSeconds();

                postData = {
                    type        : type, 
                    data        : postData, 
                    datetime    : datetime, 
                    app_version : app_version
                };

                //console.log("What are we posting");
                //console.log(postData); return false;

                return request = $http({
                    method: "post",
                    url: url,
                    data: postData,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                });
            },

            fetch : function(path, packadge) {
                return  $http.get('/application/storeemployees', packadge);
            }
        };
    })

    .service('dataService', function($q, RequestService, $cordovaSQLite, ipCookie, $ionicPopup, $state, $http) {
        return{
            sync : function(){
                var deferred = $q.defer();
                var promise = deferred.promise;

                var details = ipCookie('LOGIN', undefined, {decode: function (value) { return value; }});

                //console.warn(checknet);

                $cordovaSQLite.execute(db, "SELECT * FROM sync_data ").then(function(sync) {
                    var syncData;
                    if(sync.rows.length > 0){
                        syncData = JSON.parse(sync.rows.item(0).store_content);
                    }else{
                        syncData = 'No Data to Sync';
                    }

                    RequestService.post('sync_new',{user_id:details.id, data : syncData})
                    .success(function (result) {

                        //console.log("I want may stores", result);
                        //console.log(result['ALL_STORES']);

                        if(syncData != 'No Data to Sync') {
                            $cordovaSQLite.execute(db, "DELETE FROM sync_data ");
                        }

                        if(result['USER_STATUS'] == 'active') {

                            $cordovaSQLite.execute(db, "SELECT * FROM all_stores ").then(function(stores) {
                                if(stores.rows.length > 0){
                                    $cordovaSQLite.execute(db, "DELETE FROM all_stores ");
                                }
                                var all_stores = JSON.stringify(result['ALL_STORES']);
                                $cordovaSQLite.execute(db, "INSERT INTO all_stores (store_content) VALUES ('"+all_stores+"')");
                            });

                            $cordovaSQLite.execute(db, "SELECT * FROM list_stores ").then(function(list) {
                                if(list.rows.length > 0){
                                    $cordovaSQLite.execute(db, "DELETE FROM list_stores ");
                                }
                                var list_stores = JSON.stringify(result['LIST_STORES']);
                                $cordovaSQLite.execute(db, "INSERT INTO list_stores (store_content) VALUES ('"+list_stores+"')");
                            });

                            /*
                            $cordovaSQLite.execute(db, "SELECT * FROM store_employees ").then(function(list) {
                                if(list.rows.length > 0){
                                    $cordovaSQLite.execute(db, "DELETE FROM store_employees ");
                                }
                                var list_employees = JSON.stringify(result['ALL_STORES']);
                                $cordovaSQLite.execute(db, "INSERT INTO store_employees (store_content) VALUES ('"+list_employees+"')");
                            });
                            */

                            ipCookie('WORLD_SCORE', result['WORLD_SCORE'], { expires: 1000, encode: function (value) { return value; } });
                            ipCookie('COMPETITORS', result['COMPETITORS'], { expires: 1000, encode: function (value) { return value; } });
                        }

                        deferred.resolve(result['USER_STATUS']);
                    })
                    .error(function(err) {
                        //alert("dataService: user id: " + details.id + " data: " + JSON.stringify(syncData));
                        $cordovaSQLite.execute(db, "SELECT * FROM list_stores ")
                        .then(function(stores) {
                            if(stores.rows.length > 0){
                                deferred.resolve(stores.rows.item(0).store_content);
                            }
                        });

                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title"> Something went wrong!</div>',
                            template:'<div class="popup-subtitle">There was no data to Sync or Please check your Internet Connection</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    type: 'button-pmi',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    });
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                };
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                };
                return promise;
            },

            stores : function(){
                var deferred = $q.defer();
                var promise = deferred.promise;

                $cordovaSQLite.execute(db, "SELECT * FROM list_stores ").then(function(stores) {
                    if(stores.rows.length > 0){
                        deferred.resolve(stores.rows.item(0).store_content);
                    }
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                };
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                };
                return promise;
            },
            
            store : function(ID){
                var deferred = $q.defer();
                var promise = deferred.promise;

                $cordovaSQLite.execute(db, "SELECT * FROM all_stores ").then(function(store) {
                    if(store.rows.length > 0){
                        var strData = store.rows.item(0).store_content;
                        deferred.resolve(strData);
                    }
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                };
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                };
                return promise;
            },
            
            world : function(ID){
                return ipCookie('WORLD_SCORE', undefined, {decode: function (value) { return value; }});
            },
            
            competitor : function(ID){
                return ipCookie('COMPETITORS', undefined, {decode: function (value) { return value; }});
            },
            
            updateQ : function(NewStats, Store, Year, Month){
                var deferred = $q.defer();
                var promise = deferred.promise;

                //console.log("NewStats");
                //console.log(NewStats); return false;

                $cordovaSQLite.execute(db, "SELECT * FROM sync_data ").then(function(sync) {
                    if(sync.rows.length > 0){
                        var syncData = JSON.parse(sync.rows.item(0).store_content);
                    } else {
                        var syncData = {};
                    }

                    if(!syncData['DATA'])
                        syncData['DATA'] = {};
                    
                    if(!syncData['DATA'][Store])

                        syncData['DATA'][Store] = {};
                    
                    if(!syncData['DATA'][Store][Year])
                        syncData['DATA'][Store][Year] = {};
                    
                    if(!syncData['DATA'][Store][Year][Month])
                        syncData['DATA'][Store][Year][Month] = {};
                    
                    syncData['DATA'][Store][Year][Month]['update'] = NewStats;

                    //console.log("NewStats"); 
                    //console.log(syncData); return false; /*CHECKED !!*/

                    $cordovaSQLite.execute(db, "SELECT * FROM all_stores ").then(function(stores) {
                        var storesData = JSON.parse(stores.rows.item(0).store_content);

                        storesData[Store]['CURRENT_STATS'][Year][Month] = NewStats;

                        $cordovaSQLite.execute(db, "DELETE FROM all_stores ");
                        $cordovaSQLite.execute(db, "DELETE FROM sync_data ");

                        var all_stores = JSON.stringify(storesData);
                        $cordovaSQLite.execute(db, "INSERT INTO all_stores (store_content) VALUES ('"+all_stores+"')");

                        var sync_db = JSON.stringify(syncData);
                        $cordovaSQLite.execute(db, "INSERT INTO sync_data (store_content) VALUES ('"+sync_db+"')");

                        deferred.resolve('1');
                    });
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                };
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                };
                return promise;
            },
            
            start : function(ID){
                var deferred = $q.defer();
                var promise = deferred.promise;

                var currentdate = new Date();
                var currentMonth = currentdate.getMonth()+1;
                var currentYear = currentdate.getFullYear();
                var StartingObj = {
                    1 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    2 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    3 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    4 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    5 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    6 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    7 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    8 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    9 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    10 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    11 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    12 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    },
                    13 :{
                        V1A : null,
                        V1DT : null,
                        V1I : null,
                        V1C : null,
                        V2A : null,
                        V2DT : null,
                        V2I : null,
                        V2C : null,
                        V3A : null,
                        V3DT : null,
                        V3I : null,
                        V3C : null,
                        V4A : null,
                        V4DT : null,
                        V4I : null,
                        V4C : null
                    }
                }

                $cordovaSQLite.execute(db, "SELECT * FROM sync_data ").then(function(sync) {
                    if(sync.rows.length > 0){
                        var syncData = JSON.parse(sync.rows.item(0).store_content);
                    }else
                    {
                        var syncData = {};
                    }

                    if(!syncData['DATA'])
                        syncData['DATA'] = {};
                    if(!syncData['DATA'][ID])
                        syncData['DATA'][ID] = {};
                    if(!syncData['DATA'][ID][currentYear])
                        syncData['DATA'][ID][currentYear] = {};
                    if(!syncData['DATA'][ID][currentYear][currentMonth])
                        syncData['DATA'][ID][currentYear][currentMonth] = {};
                    syncData['DATA'][ID][currentYear][currentMonth]['start'] = '1';

                    $cordovaSQLite.execute(db, "SELECT * FROM all_stores ").then(function(stores) {
                        var storesData = JSON.parse(stores.rows.item(0).store_content);
                        $cordovaSQLite.execute(db, "DELETE FROM all_stores ");
                        $cordovaSQLite.execute(db, "DELETE FROM sync_data ");

                        storesData[ID]['CURRENT_STATS'] = {};
                        storesData[ID]['CURRENT_STATS'][currentYear] = {};
                        storesData[ID]['CURRENT_STATS'][currentYear][currentMonth] = StartingObj;
                        storesData[ID]['CALL_STATUS'] = "Continuer";

                        var all_stores = JSON.stringify(storesData);
                        $cordovaSQLite.execute(db, "INSERT INTO all_stores (store_content) VALUES ('"+all_stores+"')");
                        var sync_db = JSON.stringify(syncData);
                        $cordovaSQLite.execute(db, "INSERT INTO sync_data (store_content) VALUES ('"+sync_db+"')");

                        deferred.resolve(storesData[ID]);
                    });
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },
            
            complete : function(ID, Year, Month){
                var deferred = $q.defer();
                var promise = deferred.promise;
                var currentdate = new Date();
                var currentMonth = currentdate.getMonth()+1;
                var currentYear = currentdate.getFullYear();

                $cordovaSQLite.execute(db, "SELECT * FROM sync_data ").then(function(sync) {
                    if(sync.rows.length > 0){
                        var syncData = JSON.parse(sync.rows.item(0).store_content);
                    }else
                    {
                        var syncData = {};
                    }

                    if(!syncData['DATA'])
                        syncData['DATA'] = {};
                    if(!syncData['DATA'][ID])
                        syncData['DATA'][ID] = {};
                    if(!syncData['DATA'][ID][Year])
                        syncData['DATA'][ID][Year] = {};
                    if(!syncData['DATA'][ID][Year][Month])
                        syncData['DATA'][ID][Year][Month] = {};
                    syncData['DATA'][ID][Year][Month]['completed'] = '1';

                    $cordovaSQLite.execute(db, "SELECT * FROM all_stores ").then(function(stores) {
                        var storesData = JSON.parse(stores.rows.item(0).store_content);
                        if(!storesData[ID]['PAST_STATS'][Year])
                            storesData[ID]['PAST_STATS'][Year] = {};
                        storesData[ID]['PAST_STATS'][Year][Month] = storesData[ID]['CURRENT_STATS'][Year][Month];
                        storesData[ID]['CURRENT_STATS'] = [];

                        if(currentYear > Year){
                            storesData[ID]['CALL_STATUS'] = "Démarrer";
                        }else{
                            if(currentMonth > Month){
                                storesData[ID]['CALL_STATUS'] = "Démarrer";
                            }else{
                                storesData[ID]['CALL_STATUS'] = "Terminé";
                            }
                        }

                        $cordovaSQLite.execute(db, "DELETE FROM all_stores ");
                        $cordovaSQLite.execute(db, "DELETE FROM sync_data ");

                        var all_stores = JSON.stringify(storesData);
                        $cordovaSQLite.execute(db, "INSERT INTO all_stores (store_content) VALUES ('"+all_stores+"')");
                        var sync_db = JSON.stringify(syncData);
                        $cordovaSQLite.execute(db, "INSERT INTO sync_data (store_content) VALUES ('"+sync_db+"')");

                        deferred.resolve(storesData[ID]);
                    });
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },
            
            updatePos : function(lat, long, Store){
                var deferred = $q.defer();
                var promise = deferred.promise;

                $cordovaSQLite.execute(db, "SELECT * FROM sync_data ").then(function(sync) {
                    if(sync.rows.length > 0){
                        var syncData = JSON.parse(sync.rows.item(0).store_content);
                    }else
                    {
                        var syncData = {};
                    }

                    if(!syncData['POSITION'])
                        syncData['POSITION'] = {};
                    if(!syncData['POSITION'][Store])
                        syncData['POSITION'][Store] = {};
                    syncData['POSITION'][Store]['LAT'] = lat;
                    syncData['POSITION'][Store]['LONG'] = long;

                    $cordovaSQLite.execute(db, "SELECT * FROM all_stores ").then(function(stores) {
                        var storesData = JSON.parse(stores.rows.item(0).store_content);
                        storesData[Store]['POSITION']['LAT'] = lat;
                        storesData[Store]['POSITION']['LONG'] = long;
                        $cordovaSQLite.execute(db, "DELETE FROM all_stores ");
                        $cordovaSQLite.execute(db, "DELETE FROM sync_data ");

                        var all_stores = JSON.stringify(storesData);
                        $cordovaSQLite.execute(db, "INSERT INTO all_stores (store_content) VALUES ('"+all_stores+"')");
                        var sync_db = JSON.stringify(syncData);
                        $cordovaSQLite.execute(db, "INSERT INTO sync_data (store_content) VALUES ('"+sync_db+"')");

                        deferred.resolve('1');
                    });
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },
            
            updateComp : function(NewStats, Store){
                var deferred = $q.defer();
                var promise = deferred.promise;

                var currentdate = new Date();
                var datetime =  currentdate.getFullYear() +"."
                    + (currentdate.getMonth()+1) +"."
                    + currentdate.getDate() + "."
                    + currentdate.getHours() + "."
                    + currentdate.getMinutes() + "."
                    + currentdate.getSeconds();

                $cordovaSQLite.execute(db, "SELECT * FROM sync_data ").then(function(sync) {
                    if(sync.rows.length > 0){
                        var syncData = JSON.parse(sync.rows.item(0).store_content);
                    }else
                    {
                        var syncData = {};
                    }

                    if(!syncData['COMPETITOR'])
                        syncData['COMPETITOR'] = {};
                    if(!syncData['COMPETITOR'][Store])
                        syncData['COMPETITOR'][Store] = {};
                    if(!syncData['COMPETITOR'][Store][datetime])
                        syncData['COMPETITOR'][Store][datetime] = {};
                    syncData['COMPETITOR'][Store][datetime] = NewStats;

                    $cordovaSQLite.execute(db, "DELETE FROM sync_data ");
                    var sync_db = JSON.stringify(syncData);
                    $cordovaSQLite.execute(db, "INSERT INTO sync_data (store_content) VALUES ('"+sync_db+"')");

                    deferred.resolve('1');
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            getImageStore : function(store_id) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                $cordovaSQLite.execute(db, "SELECT * FROM storeIMG").then(function(sync) {
                     deferred.resolve(sync);
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },
            
            getImageFurniture : function(store_id) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                $cordovaSQLite.execute(db, "SELECT * FROM furniture").then(function(sync) {
                     deferred.resolve(sync);
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            saveStoreIMG : function(store_id, store_IMG) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                var syncData = {};

                syncData.STOREID  = store_id;
                syncData.IMAGE = store_IMG;

                //if(!isEmpty(syncData)){
                    $cordovaSQLite.execute(db, "INSERT INTO storeIMG (store_content) VALUES ('"+JSON.stringify(syncData)+"')")
                        .then(function (result) {
                            deferred.resolve(result);
                    }, function (err) {
                        console.error(err);
                        });
               // }

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            saveFurnitureIMG : function(store_id, store_IMG) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                var syncData = {};

                syncData.STOREID  = store_id;
                syncData.IMAGE = store_IMG;

                //if(!isEmpty(syncData)){
                    $cordovaSQLite.execute(db, "INSERT INTO furniture (store_content) VALUES ('"+JSON.stringify(syncData)+"')")
                        .then(function (result) {
                            deferred.resolve(result);
                    }, function (err) {
                        console.error(err);
                        });
               // }

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            syncEmployees : function(){
                var deferred = $q.defer();
                var promise = deferred.promise;

                     $cordovaSQLite.execute(db, "SELECT * FROM collect_info").then(function(sync) {

                         if(sync.rows.length > 0){
                             
                            if(checknet == 'online') {

                            var newsyncData = {};

                            for(n = 0; n < sync.rows.length; n++){

                                newsyncData[n] = JSON.parse(sync.rows.item(n).store_content);
                                // console.log(JSON.parse(sync.rows.item(n).store_content)); 
                            
                            }

                                RequestService.post('employees', newsyncData)
                                .success( function(result){
                                    return result;
                                })
                                .error( function(error){
                                    return error;
                                })
                            }
                         }
                  
                    });

                    promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            syncStoreIMAGE : function(){
                var deferred = $q.defer();
                var promise = deferred.promise;

                     $cordovaSQLite.execute(db, "SELECT * FROM storeIMG").then(function(sync) {

                         if(sync.rows.length > 0){
                             
                            if(checknet == 'online') {

                            var newsyncData = {};

                            for(n = 0; n < sync.rows.length; n++){

                                newsyncData[n] = JSON.parse(sync.rows.item(n).store_content);
                                // console.log(JSON.parse(sync.rows.item(n).store_content)); 
                            
                            }

                                RequestService.post('storeIMG', newsyncData)
                                .success( function(result){
                                    return result;
                                })
                                .error( function(error){
                                    return error;
                                })
                            }
                         }
                  
                    });

                    promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            syncFurnitureIMAGE : function(){
                var deferred = $q.defer();
                var promise = deferred.promise;

                     $cordovaSQLite.execute(db, "SELECT * FROM furniture").then(function(sync) {

                         if(sync.rows.length > 0){
                             
                            if(checknet == 'online') {

                            var newsyncData = {};

                            for(n = 0; n < sync.rows.length; n++){

                                newsyncData[n] = JSON.parse(sync.rows.item(n).store_content);
                                // console.log(JSON.parse(sync.rows.item(n).store_content)); 
                            
                            }

                                RequestService.post('furniture', newsyncData)
                                .success( function(result){
                                    return result;
                                })
                                .error( function(error){
                                    return error;
                                })
                            }
                         }
                  
                    });

                    promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            collectInfo : function(store_id, manager_info, cashier_01, cashier_02, cashier_03) {
                //console.log("Did I even get here ? Saving Collect Info :: ");
                //console.warn(store_id); return false;
                var deferred = $q.defer();
                var promise = deferred.promise;
                var syncData = {};

                syncData.STOREID  = store_id;
                syncData.MANAGERS = manager_info;
                // syncData.CASHIERS = {cashier_01, cashier_02, cashier_03};
                syncData.CASHIER_01 = cashier_01;
                syncData.CASHIER_02 = cashier_02;
                syncData.CASHIER_03 = cashier_03;
                
                //NOTES
                // App employee data loses state when reset
                //values for cashier_01 etc in the form after submitting is the same for each store
                //

                if(!isEmpty(syncData)){
                    $cordovaSQLite.execute(db, "INSERT INTO collect_info (store_content) VALUES ('"+JSON.stringify(syncData)+"')")
                        .then(function (result) {
                            deferred.resolve(result);
                    }, function (err) {
                        console.error(err);
                        });
                }
                
                $cordovaSQLite.execute(db, "SELECT * FROM collect_info").then(function(sync) {
                    
                    console.log(sync);

                    if(checknet == 'online') {

                     var newsyncData = {};

                       for(n = 0; n < sync.rows.length; n++){

                        newsyncData[n] = JSON.parse(sync.rows.item(n).store_content);
                        // console.log(JSON.parse(sync.rows.item(n).store_content)); 
                    
                       }

                        RequestService.post('employees', newsyncData)
                        .success( function(result){
                            return result;
                        })
                        .error( function(error){
                            return error;
                        })
                    }
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },

            storeEmployees : function(store_id) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                var syncData = {};

               $cordovaSQLite.execute(db, "SELECT * FROM all_stores ").then(function(stores) {
                    //console.log( JSON.parse(stores.rows.item(0).store_content) );
                    if(stores.rows.length > 0){
                        deferred.resolve(JSON.parse(stores.rows.item(0).store_content));
                    }
                });

                promise.success = function(fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function(fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            }

        }
    })

    .service('LoaderService', function($ionicLoading) {
        return{
            showLoading : function(text){
                text = '<ion-spinner icon="lines" class="spinner-pmi"></ion-spinner><br />' + text || '<ion-spinner icon="lines"></ion-spinner>';
                $ionicLoading.show({
                    template: text,
                });
            },
            hideLoading : function(){
                $ionicLoading.hide();
            }
        }
    });