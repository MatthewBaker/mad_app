/**
 * Created by anele on 2017/07/13.
 */


/*
 *	This is where the magic happens
 * =============================================================================
 */

(function(){
    'use strict';

    angular.module('starter')
        .constant('JQ_CONFIG', {

        })
        .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {

            $ocLazyLoadProvider.config({
                debug 	: false,
                events 	: true,
                modules	: []
            });
        }]);
})();
