/**
 * Created by anele on 2017/07/13.
 */

/*
 *  Run our configs
 * =============================================================================
 */

var app =
    angular.module('starter')

    .run( function ($ionicPlatform, $cordovaSQLite, $cordovaNetwork, $rootScope, $window, $state) {
        'use strict';

        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }

            // CHECK CONNECTION
            if(window.Connection){
                // ON STARTUP
                if($cordovaNetwork.isOnline()){
                    checknet = 'online';
                }else if($cordovaNetwork.isOffline()){
                    checknet = 'offline';
                }

                // listen for Online event
                $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                    checknet = 'online';
                });

                // listen for Offline event
                $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                    checknet = 'offline';
                });
            }

            // HIDE STATUSBAR
            if(window.StatusBar) {
                StatusBar.hide();
                ionic.Platform.fullScreen();
            }

            // HIDE SPLASHSCREEN AFTER APP LOAD
            if(navigator.splashscreen){
                setTimeout(function() {
                    navigator.splashscreen.hide();
                }, 1000);
            }

            if (window.cordova) {
                db = $cordovaSQLite.openDB({ name: "my.db" });
            }
            else {
                db = window.openDatabase("my.db", "1.0", "MyDatabase", -1);
            }

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS all_stores (id integer primary key, store_content text)");
            //$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS store_employees (id integer primary key, store_content text)");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS list_stores (id integer primary key, store_content text)");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS sync_data (id integer primary key, store_content text)");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS collect_info (id integer primary key, store_content text)");
            //
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS storeIMG (id integer primary key, store_content blob)");
            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS furniture (id integer primary key, store_content blob)");
        });
    });