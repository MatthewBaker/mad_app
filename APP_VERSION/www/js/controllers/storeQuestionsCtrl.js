/**
 * Created by anele on 2017/07/13.
 */


/* =============================================================================
 *  SoresCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('storeQuestionsCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService, $ionicPopup, ipCookie){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    LoaderService.showLoading('Loading...');
                    $scope.store = $scope.layout.store;
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Home',link:'app.storeHome'},
                        {title:'Questions',link:''}
                    ];

                    dataService.store().success(function (result) {
                        var serviceStats = JSON.parse(result);
                        $scope.page = {};
                        $scope.page.currentStats = serviceStats[$scope.layout.store.id]['CURRENT_STATS'] ;
                        $scope.commentvalue = '';
                        $scope.visit_val = '';
                        $scope.question_val = '';
                        $scope.visitImage = '';
                        $scope.visitComment = '';
                        $scope.disabled = false;

                        $scope.current_image_store = [];

                        $scope.visit = '1';
                        $scope.visitAnswer = '';
                        $scope.imgSRC = 'img/placeholder.png';

                        angular.forEach($scope.page.currentStats,function(value, key) {
                            $scope.currentYear = key;
                            $scope.layout.year = key;
                        });

                        angular.forEach($scope.page.currentStats[$scope.currentYear],function(value, key) {
                            $scope.currentMonth = key;
                            $scope.layout.month = key;
                        });

                        $scope.points = 0;
                        $scope.update = 0;
                        $scope.submitMonth = null;
                        $scope.submitYear = null;
                        $scope.submision_text = 'Update';
                        $scope.question_number = Object.keys($scope.page.currentStats[$scope.currentYear][$scope.currentMonth]).length;

                        // CHECK DATA
                        checkData();
                        checkStatus();
                        LoaderService.hideLoading();
                    });
                });

                // BACK TO DETAILS
                $scope.backToHome = function(){
                    $state.go('app.storeHome');
                };

                $scope.check = function(Visit, Question){
                    $scope.update = 1;
                    if(Question == '13')
                        $scope.submision_text = 'Submit Visit';
                    else
                        $scope.submision_text = 'Update';

                    var currentdate = new Date();
                    var datetime =  currentdate.getFullYear() +"-"
                        + (currentdate.getMonth()+1) +"-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][Question]['V'+Visit+'DT'] = datetime;
                    checkData();
                };

                $scope.updateQ = function(){
                    $scope.update = 0;
                    LoaderService.showLoading('Updating Data');
                    checkStatus();
                    dataService.updateQ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth], $scope.layout.store.id,$scope.currentYear,$scope.currentMonth).success(function (result) {
                        if(checknet == 'online'){
                            dataService.sync().success(function (result) {
                                LoaderService.hideLoading();
                                if(result != 'active'){
                                    ipCookie.remove('LOGIN');
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Update Questions Synching</div>',
                                        template:'<div class="popup-subtitle">Incorrect login details</div>',
                                        buttons: [
                                            {
                                                text: 'To login page',
                                                onTap: function () {
                                                    $state.go('login');
                                                }
                                            }
                                        ]
                                    });
                                }
                            }).error(function(){
                                LoaderService.hideLoading();
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Update questions Sync Failed</div>',
                                    template:'<div class="popup-subtitle">Data Error</div>',
                                    buttons: [
                                        {
                                            text: 'To login page',
                                            onTap: function () {
                                                $state.go('login');
                                            }
                                        }
                                    ]
                                });
                            });
                        }else{
                            LoaderService.hideLoading();
                        }
                    }).error(function(){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Update Questions Failed</div>',
                            template:'<div class="popup-subtitle">Data Error</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    });
                };

                $scope.submit = function(){
                    LoaderService.showLoading('Submiting Data');
                    dataService.complete($scope.layout.store.id,$scope.currentYear,$scope.currentMonth).success(function (result) {
                        if(checknet == 'online'){
                            LoaderService.showLoading('Loading Data');
                            dataService.sync().success(function (result2) {
                                LoaderService.hideLoading();
                                if(result2 != 'active'){
                                    ipCookie.remove('LOGIN');
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Submit Questions Synching</div>',
                                        template:'<div class="popup-subtitle">Incorrect login details</div>',
                                        buttons: [
                                            {
                                                text: 'To login page',
                                                onTap: function () {
                                                    $state.go('login');
                                                }
                                            }
                                        ]
                                    });
                                }else{
                                    dataService.store().success(function (result) {
                                        var returnSubmitteddata = JSON.parse(result);

                                        $scope.layout.store.current_stats = returnSubmitteddata[$scope.layout.store.id]['CURRENT_STATS'];
                                        $scope.layout.store.past_stats = returnSubmitteddata[$scope.layout.store.id]['PAST_STATS'];
                                        $scope.layout.store.call_status = returnSubmitteddata[$scope.layout.store.id]['CALL_STATUS'];
                                        $scope.layout.world = dataService.world();

                                        LoaderService.hideLoading();
                                        $scope.layout.call_completed = true;
                                        $state.go('app.storeSummary');
                                    });
                                }
                            }).error(function(){
                                LoaderService.hideLoading();
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Submit questions Sync Failed</div>',
                                    template:'<div class="popup-subtitle">Data Error</div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                            });
                        }else{
                            dataService.store().success(function (result) {
                                var returnSubmitteddata = JSON.parse(result);

                                $scope.layout.store.current_stats = returnSubmitteddata[$scope.layout.store.id]['CURRENT_STATS'];
                                $scope.layout.store.past_stats = returnSubmitteddata[$scope.layout.store.id]['PAST_STATS'];
                                $scope.layout.store.call_status = returnSubmitteddata[$scope.layout.store.id]['CALL_STATUS'];
                                $scope.layout.world = dataService.world();

                                LoaderService.hideLoading();
                                $state.go('app.storeSummary');
                            });
                        }
                    }).error(function(){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Submiting data Failed</div>',
                            template:'<div class="popup-subtitle">Data Error</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    });
                };

                function checkData(){
                    $scope.points = 0;
                    angular.forEach($scope.page.currentStats[$scope.currentYear][$scope.currentMonth],function(value, key) {
                        var V1P3 = (value.V1A == 'yes') ? 1 : 0 ;
                        var V2P3 = (value.V2A == 'yes') ? 1 : 0 ;
                        var V3P3 = (value.V3A == 'yes') ? 1 : 0 ;
                        var V4P3 = (value.V4A == 'yes') ? 1 : 0 ;
                        $scope.points += V1P3 + V2P3 + V3P3 + V4P3;
                    });
                }

                function checkStatus(){
                    var visit1Check = true;
                    var visit2Check = true;
                    var visit3Check = true;
                    var visit4Check = true;
                    angular.forEach($scope.page.currentStats[$scope.currentYear][$scope.currentMonth],function(value, key) {
                        if(value.V1A != 'yes' && value.V1A != 'no' && value.V1A != 'na'){
                            visit1Check = false;
                        }
                        if(value.V2A != 'yes' && value.V2A != 'no' && value.V2A != 'na'){
                            visit2Check = false;
                        }
                        if(value.V3A != 'yes' && value.V3A != 'no' && value.V3A != 'na'){
                            visit3Check = false;
                        }
                        if(value.V4A != 'yes' && value.V4A != 'no' && value.V4A != 'na'){
                            visit4Check = false;
                        }
                    });

                    if(!visit1Check){
                        $scope.visit = '1';
                        $scope.visitAnswer = 'V1A';
                        $scope.visitImage = 'V1I';
                        $scope.visitComment = 'V1C';
                    }else if(!visit2Check){
                        $scope.visit = '2';
                        $scope.visitAnswer = 'V2A';
                        $scope.visitImage = 'V2I';
                        $scope.visitComment = 'V2C';
                    }else if(!visit3Check){
                        $scope.visit = '3';
                        $scope.visitAnswer = 'V3A';
                        $scope.visitImage = 'V3I';
                        $scope.visitComment = 'V3C';
                    }else if(!visit4Check){
                        $scope.visit = '4';
                        $scope.visitAnswer = 'V4A';
                        $scope.visitImage = 'V4I';
                        $scope.visitComment = 'V4C';
                    }else{
                        $scope.visit = '4';
                        $scope.visitAnswer = 'V4A';
                        $scope.visitImage = 'V4I';
                        $scope.visitComment = 'V4C';
                        $scope.disabled = true;
                        $scope.submitMonth = 1;
                    }
                }

                $scope.commentModal = function(stats, question, visit){
                    $scope.comment_modal = stats;

                    if(stats == 'true')
                    {
                        $scope.visit_val = visit;
                        $scope.question_val = question;
                        $scope.commentvalue = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'];
                    }else{
                        $scope.commentvalue = '';
                    }
                }

                $scope.submitCommentModal = function(comment ,question, visit){
                    $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] = comment;

                    // $scope.updateQ();
                    $scope.comment_modal = 'false';
                };

                $scope.takePicture = function(question, visit){
                    LoaderService.showLoading('Taking Picture...');
                    var currentdate = new Date();
                    var datetime =  currentdate.getFullYear() +"-"
                        + (currentdate.getMonth()+1) +"-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    navigator.camera.getPicture(onSuccess, onFail, { quality: 100,targetWidth: 480,targetHeight: 480,
                        destinationType: Camera.DestinationType.DATA_URL
                    });

                    function onSuccess(imageData) {
                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] == '' || $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] == null)
                            $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] = "data:image/jpeg;base64," + imageData;
                        else
                            $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] + "|data:image/jpeg;base64," + imageData;
                        LoaderService.hideLoading();
                        // $scope.submitImageModal(question, visit);
                        $scope.imageModal('true',question, visit);
                    }

                    function onFail(message) {
                        LoaderService.hideLoading();
                        alert('Failed because: ' + message);
                    }
                };

                $scope.imageModal = function(stats, question, visit){
                    $scope.current_image_store = [];
                    $scope.image_modal = stats;
                    $scope.visit_val = visit;
                    $scope.question_val = question;
                    if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != null){
                        $scope.imgSRC = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'];
                        $scope.current_image_store = $scope.imgSRC.split('|');
                    }else{
                        $scope.imgSRC = 'img/placeholder.png';
                    }
                };

                $scope.submitImageModal = function(question, visit){
                    $scope.image_modal = 'false';
                }
            });
        })

})();