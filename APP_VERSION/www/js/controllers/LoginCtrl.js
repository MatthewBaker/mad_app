/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *  LoginCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('LoginCtrl', function($ionicPlatform, $scope, $state, RequestService, $ionicPopup, ipCookie, dataService, LoaderService){
            $ionicPlatform.ready(function() {
                // INPUT DATA
                $scope.data = {};
                $scope.app_version = app_version;

                // LOGIN FUNCTION
                $scope.login = function(){
                    if($scope.data.login_id == '' || $scope.data.login_id == null || $scope.data.password == '' || $scope.data.password == null){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Login Failed</div>',
                            template:'<div class="popup-subtitle">Please fill in all the fields.</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    }else{
                        // LOGIN REQUEST
                        LoaderService.showLoading('Checking credentials');
                        RequestService.post('login',{login_id:$scope.data.login_id.toLowerCase(),password:$scope.data.password}).success(function (result) {
                            if(result != 'failed'){
                                ipCookie('LOGIN', {id:result['id'],name:result['first_name']}, { expires: 1000, encode: function (value) { return value; } });
                                LoaderService.showLoading('Updating data');
                                dataService.sync().success(function (result) {
                                    LoaderService.hideLoading();
                                    if(result != 'active'){
                                        ipCookie.remove('LOGIN');
                                        $ionicPopup.show({
                                            title: '<div class="popup-pmi-title">Login Failed</div>',
                                            template:'<div class="popup-subtitle">Incorrect login details</div>',
                                            buttons: [
                                                {
                                                    text: 'OK',
                                                    type: 'button-pmi',
                                                    onTap: function () {
                                                    }
                                                }
                                            ]
                                        });
                                    }else{
                                        $state.go('app.stores');
                                    }
                                }).error(function(){
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Login Failed</div>',
                                        template:'<div class="popup-subtitle">Data Error</div>',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                type: 'button-pmi',
                                                onTap: function () {
                                                }
                                            }
                                        ]
                                    });
                                });
                            }else{
                                LoaderService.hideLoading();
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Login Failed</div>',
                                    template:'<div class="popup-subtitle">Incorrect login details</div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-pmi',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                            }
                        }).error(function(){
                            LoaderService.hideLoading();
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Login Failed</div>',
                                template:'<div class="popup-subtitle">An Unknown error occured</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        type: 'button-pmi',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        });
                    }
                }
            });
        })

})();