/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *  SoresCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('StoresCtrl', function($ionicPlatform, $scope, $state, dataService , $ionicPopup, LoaderService){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    console.log('beforeEnter');
                    LoaderService.showLoading('Loading...');
                    if(checknet == 'online'){
                        dataService.sync().success(function (result) {
                            LoaderService.hideLoading();
                            if(result != 'active'){
                                ipCookie.remove('LOGIN');
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Login Failed</div>',
                                    template:'<div class="popup-subtitle">Incorrect login details</div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-pmi',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                            }

                            $scope.layout.breadcrumbs = [];
                            $scope.stores = [];

                            dataService.stores().success(function (result) {
                                var objStores = JSON.parse(result);
                                angular.forEach(objStores,function(value, key) {
                                    var tempArrStores = { id : key, name : value};
                                    $scope.stores.push(tempArrStores);
                                });
                                $scope.layout.store = {
                                    name : '',
                                    id : ''
                                }
                            });

                        }).error(function(){
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Layout Sync Failed</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        type: 'button-pmi',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        });
                    }else{
                        $scope.layout.breadcrumbs = [];
                        $scope.stores = [];

                        dataService.stores().success(function (result) {
                            LoaderService.hideLoading();
                            var objStores = JSON.parse(result);
                            angular.forEach(objStores,function(value, key) {
                                var tempArrStores = { id : key, name : value};
                                $scope.stores.push(tempArrStores);
                            });
                            $scope.layout.store = {
                                name : '',
                                id : ''
                            }
                        });
                    }
                });

                // CHOOSE STORE
                $scope.selectStore = function(A, B){
                    dataService.store().success(function (result) {
                        var StoreData = JSON.parse(result);
                        console.log(result);
                        $scope.layout.store = {
                            name : A,
                            id : B,
                            month : '',
                            year : '',
                            current_stats : StoreData[B]['CURRENT_STATS'],
                            past_stats : StoreData[B]['PAST_STATS'],
                            call_status : StoreData[B]['CALL_STATUS'],
                            position: StoreData[B]['POSITION']
                        }

                        $scope.layout.world = dataService.world();
                        $scope.layout.competitor = dataService.competitor();
                        $state.go('app.storeDetails');
                    });
                }
            });
        })
})();