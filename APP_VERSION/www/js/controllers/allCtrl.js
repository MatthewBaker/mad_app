/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *  AllCtrl - Combined
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

    // CONFIG SETUP
        .controller('ConfigCtrl', function($ionicPlatform, $scope, $state, $ionicPopup, ipCookie, dataService){ 
            $ionicPlatform.ready(function() {
                //console.log(ipCookie('LOGIN')); return false;
                if(ipCookie('LOGIN')){
                    $scope.user = ipCookie('LOGIN', undefined, {decode: function (value) { return value; }});
                    $scope.feedback = 'Welcome ' + $scope.user.name + ", Loading Stores...";
                    if(checknet == 'online'){
                        
                        dataService.sync()
                        .success(function (result) {
                            if(result != 'active'){
                                ipCookie.remove('LOGIN');
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                                    template:'<div class="popup-subtitle">Incorrect login details</div>',
                                    buttons: [
                                        {
                                            text: 'To login page',
                                            onTap: function () {
                                                $state.go('login');
                                            }
                                        }
                                    ]
                                });
                            }else{

                                $state.go('app.stores');
                            }
                        })
                        .error(function(error){
                            
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'To login page',
                                        onTap: function () {
                                            $state.go('login');
                                        }
                                    }
                                ]
                            });
                            ipCookie.remove('LOGIN');
                        });
                    }else{
                        $state.go('app.stores');
                    }
                }else{
                    $scope.feedback = "Loading App...";
                    $state.go('login');
                }
            });
        })

        // LOGIN ::LoginCtrl
        .controller('LoginCtrl', function($ionicPlatform, $scope, $state, RequestService, $ionicPopup, ipCookie, dataService, LoaderService){
            $ionicPlatform.ready(function() {
                //console.log(ipCookie('LOGIN')); return false;
                // INPUT DATA
                $scope.data = {};
                $scope.app_version = app_version;

                // LOGIN FUNCTION
                $scope.login = function() {

                    if($scope.data.login_id == '' || $scope.data.login_id == null || $scope.data.password == '' || $scope.data.password == null){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Login Failed</div>',
                            template:'<div class="popup-subtitle">Please fill in all the fields.</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                        return false;
                    } //else {
                        // LOGIN REQUEST
                        LoaderService.showLoading('Checking credentials');

                        RequestService.post
                            ('login',{login_id:$scope.data.login_id.toLowerCase(),password:$scope.data.password})
                        .success(function (result) {

                            if(result != 'failed') {

                                ipCookie('LOGIN', {id:result['id'],name:result['first_name']}, { expires: 1000, encode: function (value) { return value; } });

                                LoaderService.showLoading('Updating data');

                                dataService.sync()
                                .success(function(result){
                                    dataService.sync()
                                    .success(function( result){ 
                                        $state.go('app.stores');
                                    });
                                });

                                
                                /*dataService.sync()
                                .success(function (result) {
                                    LoaderService.hideLoading();
                                    if(result != 'active'){
                                        ipCookie.remove('LOGIN');
                                        $ionicPopup.show({
                                            title: '<div class="popup-pmi-title">Login Failed</div>',
                                            template:'<div class="popup-subtitle">Incorrect login details</div>',
                                            buttons: [
                                                {
                                                    text: 'OK',
                                                    type: 'button-pmi',
                                                    onTap: function () {
                                                    }
                                                }
                                            ]
                                        });
                                    }else{
                                        $state.go('app.stores');
                                    }
                                })
                                .error(function(error){
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Login Failed</div>',
                                        template:'<div class="popup-subtitle">Data Error</div>',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                type: 'button-pmi',
                                                onTap: function () {
                                                }
                                            }
                                        ]
                                    });
                                });*/
                            }else{
                                LoaderService.hideLoading();
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Login Failed</div>',
                                    template:'<div class="popup-subtitle">Incorrect login details</div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-pmi',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                            }
                        })
                        .error(function(error){
                            //console.log(error);
                            LoaderService.hideLoading();
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Login Failed</div>',
                                template:'<div class="popup-subtitle">Incorrect login details</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        type: 'button-pmi',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        });
                    //}
                };
            });
        })

        // LAYOUT :: LayoutCtrl
        .controller('LayoutCtrl', function($ionicPlatform, $scope, $state, LoaderService, $ionicPopup, ipCookie, $rootScope, dataService){
            $ionicPlatform.ready(function() {
                $scope.layout = {
                    breadcrumbs : [],
                    store :{
                        name : '',
                        id : '',
                        month : '',
                        current_stats : '',
                        past_stats : '',
                        call_status : '',
                        position : ''
                    },
                    user : '',
                    world : '',
                    call_completed : false
                };

                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.layout.user =  ipCookie('LOGIN', undefined, {decode: function (value) { return value; }});
                    $scope.netstate = checknet;
                    $scope.app_version = app_version;
                });

                if(window.Connection){
                    // listen for Online event
                    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                        $scope.netstate = 'online';
                        LoaderService.showLoading('Synching data');
                        dataService.sync().success(function (result) {
                            if(result != 'active'){
                                ipCookie.remove('LOGIN');
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                                    template:'<div class="popup-subtitle">Incorrect login details</div>',
                                    buttons: [
                                        {
                                            text: 'To login page',
                                            onTap: function () {
                                                $state.go('login');
                                            }
                                        }
                                    ]
                                });
                            }
                            LoaderService.hideLoading();
                        })
                        .error(function(){
                            ipCookie.remove('LOGIN');
                            LoaderService.hideLoading();
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'To login page',
                                        onTap: function () {
                                            $state.go('login');
                                        }
                                    }
                                ]
                            });
                        });

                        dataService.syncStoreIMAGE()
                        .success( function(res){
                            console.log(res);
                        })
                        .error(function(error){
                            //alert("error at StoresCtrl");
                            $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">IMAGE SYNC</div>',
                                    template:'<div class="popup-subtitle">Data Error</div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-pmi',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                        });

                        dataService.syncFurnitureIMAGE()
                        .success( function(res){
                            console.log(res);
                        })
                        .error(function(error){
                            //alert("error at StoresCtrl");
                            $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">IMAGE SYNC</div>',
                                    template:'<div class="popup-subtitle">Data Error</div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-pmi',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                        });

                        dataService.syncEmployees()
                        .success( function(res){
                            console.log(res);
                        })
                        .error(function(error){
                            alert("error at StoresCtrl");
                        });


                    });

                    // listen for Offline event
                    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                        $scope.netstate = 'offline';
                    });
                }

                $scope.crumbGo = function(A){
                    if(A != '')
                        $state.go(A);
                };

                $scope.logout = function(){
                    $ionicPopup.show({
                        title: '<div class="popup-pmi-title">Are you sure you want to sign out?</div>',
                        buttons: [
                            {
                                text: 'Yes',
                                type: 'button-pmi',
                                onTap: function () {                                    
                                    ipCookie.remove('LOGIN');
                                    ipCookie.remove('ALL_STORES');
                                    ipCookie.remove('LIST_STORES');
                                    ipCookie.remove('WORLD_SCORE');
                                    ipCookie.remove('SYNCH');
                                    $state.go('login');
                                }
                            },
                            {
                                text: 'No',
                                type: 'button-pmi',
                                onTap: function () {

                                }
                            }
                        ]
                    });
                };
            });
        })

        // STORES :: StoresCtrl
        .controller('StoresCtrl', function($ionicPlatform, $scope, $state, dataService , $ionicPopup, LoaderService){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    LoaderService.showLoading('Loading . . .');

                    if(checknet == 'online'){
                        dataService.sync()
                        .success(function (result) {

                            $scope.layout.breadcrumbs = [];
                            $scope.stores = [];

                            // START SYNC TWO
                            dataService.sync()
                            .success(function (result){
                                dataService.stores().success(function (result) {
                                    var objStores = JSON.parse(result);
                                    angular.forEach(objStores,function(value, key) {
                                        var tempArrStores = { id : key, name : value};
                                        $scope.stores.push(tempArrStores);
                                    });
                                    $scope.layout.store = {
                                        name : '',
                                        id : ''
                                    };
                                });
                            });
                            //END SYNC TWO

                            LoaderService.hideLoading();
                            if(result != 'active'){
                                ipCookie.remove('LOGIN');
                                $ionicPopup.show({
                                    title    : '<div class="popup-pmi-title">Login Failed</div>',
                                    template :'<div class="popup-subtitle">Incorrect login details</div>',
                                    buttons : [
                                        {
                                            text: 'OK',
                                            type: 'button-pmi',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                            }
                        })
                        .error(function(error){
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Layout Sync Failed</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        type: 'button-pmi',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        });
                    } else {
                        console.log("offline or no connection")
                        $scope.layout.breadcrumbs = [];
                        $scope.stores = [];

                        /*dataService.sync()
                        .success(function(result){
                            dataService.sync()
                            .success(function( result){ */
                                dataService.stores().success(function (result) {
                                    LoaderService.hideLoading();
                                    var objStores = JSON.parse(result);
                                    angular.forEach(objStores,function(value, key) {
                                        var tempArrStores = { id : key, name : value};
                                        $scope.stores.push(tempArrStores);
                                    });
                                    $scope.layout.store = {
                                        name : '',
                                        id : ''
                                    };
                                });
                            //})
                        //})
                    }

                    dataService.syncEmployees()
                    .success( function(res){
                        console.log(res);
                    })
                    .error(function(error){
                        alert("error at StoresCtrl");
                    });

                    dataService.syncStoreIMAGE()
                    .success( function(res){
                        console.log(res);
                    })
                    .error(function(error){
                        //alert("error at StoresCtrl");
                        $ionicPopup.show({
                                title: '<div class="popup-pmi-title">IMAGE SYNC</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        type: 'button-pmi',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                    });

                    dataService.syncFurnitureIMAGE()
                    .success( function(res){
                        console.log(res);
                    })
                    .error(function(error){
                        //alert("error at StoresCtrl");
                        $ionicPopup.show({
                                title: '<div class="popup-pmi-title">IMAGE SYNC</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        type: 'button-pmi',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                    });

                    //
                    
                });

                // CHOOSE STORE
                $scope.selectStore = function(A, B){
                    dataService.store().success(function (result) {
                        var StoreData = JSON.parse(result);
                        $scope.layout.store = {
                            name : A,
                            id : B,
                            month : '',
                            year : '',
                            current_stats : StoreData[B]['CURRENT_STATS'],
                            past_stats : StoreData[B]['PAST_STATS'],
                            call_status : StoreData[B]['CALL_STATUS'],
                            position: StoreData[B]['POSITION']
                        };

                        $scope.layout.world = dataService.world();
                        $scope.layout.competitor = dataService.competitor();
                        $state.go('app.storeDetails');
                    });
                };
            });
        })

        // STORE DETAILS :: StoreDetailsCtrl
        .controller('StoreDetailsCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService, $ionicPopup){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {

                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:''},
                        {title:'Performance Dimensions',link:''}
                    ];

                    $scope.store = $scope.layout.store;

                    // CHECK CURRENT MONTH
                    var dateFunc = new Date();
                    $scope.month = dateFunc.getMonth()+1;

                    angular.forEach($scope.layout.store.current_stats,function(value, key) {
                        $scope.Statmonth = key;
                    });

                    $scope.takeFurnitureIMG = function() {
                        //console.log($scope.layout.store.id); return false;
                        LoaderService.showLoading('Taking Picture...');

                        navigator.camera.getPicture(onSuccess, onFail, { quality: 100,targetWidth: 280,targetHeight: 200,
                            destinationType: Camera.DestinationType.DATA_URL, encodingType: Camera.EncodingType.JPEG
                        });

                        function onSuccess(imageData) {
                             LoaderService.hideLoading();
                            var image = "data:image/jpeg;base64," + imageData;
                            var smallImage = document.getElementById('FsmallImage');
                            var imagPlce = document.getElementById('FimgPlceHoldr');
                                imagPlce.style.display = 'none';

                            // Unhide image elements
                            smallImage.style.display = 'block';
                            smallImage.src = image;

                            //Store localy
                            dataService.saveFurnitureIMG(
                                $scope.layout.store.id,
                                image
                            )
                            .success(function(data){
                                $ionicPopup.show({
                                    title    : '<div class="popup-pmi-title">Saving ... !</div>',
                                    template : '<div class="popup-subtitle">Saving Image Information</div>',
                                    buttons  : [
                                        {
                                            text: 'OK',
                                            onTap: function () {
                                                //$state.go('app.storeDetails');
                                            }
                                        }
                                    ]
                                });
                            })
                            .error(function(error){
                                alert("Could not save image to Local storage");
                            });

                             // Retrieve the image from Local
                            dataService.getImageFurniture($scope.layout.store.id)
                            .success(function(imageData){
                                //alert('you made it here');

                                for(var n = 0; n < imageData.rows.length; n++){
                                    var data = JSON.parse(imageData.rows.item(n).store_content);

                                    if(data.STOREID == $scope.layout.store.id){
                                       var img = data.IMAGE;
                                    }
                                }

                                //alert('you made it here');
                                //alert(img);
                                var smallImage = document.getElementById('FsmallImage');
                                var imagPlce = document.getElementById('FimgPlceHoldr');
                                imagPlce.style.display = 'none';

                            // Unhide image elements
                            smallImage.style.display = 'block';
                            smallImage.style.width = '140px';
                            smallImage.style.height = '100px';
                            smallImage.src = img;
                                
                            })
                            .error(function(error){
                                alert("Could not get image from Localstorage");
                            });

                            dataService.syncFurnitureIMAGE()
                            .success( function(res){
                                console.log(res);
                            })
                            .error(function(error){
                                //alert("error at StoresCtrl");
                                $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">IMAGE SYNC</div>',
                                        template:'<div class="popup-subtitle">Data Error</div>',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                type: 'button-pmi',
                                                onTap: function () {
                                                }
                                            }
                                        ]
                                    });
                            });

                            
                        }

                        function onFail(message) {
                            LoaderService.hideLoading();
                            //alert('Failed because: ' + message);
                            $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Image Captre Failed</div>',
                                    template:'<div class="popup-subtitle"> '+ message +' </div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            onTap: function () {
                                                //$state.go('login');
                                            }
                                        }
                                    ]
                                });
                        }
                    };

                    $scope.takeStoreIMG = function() {
                        //console.log($scope.layout.store.id); return false;
                        LoaderService.showLoading('Taking Picture...');

                        navigator.camera.getPicture(onSuccess, onFail, { quality: 100,targetWidth: 280,targetHeight: 200,
                            destinationType: Camera.DestinationType.DATA_URL, encodingType: Camera.EncodingType.JPEG
                        });

                        function onSuccess(imageData) {
                             LoaderService.hideLoading();
                            var image = "data:image/jpeg;base64," + imageData;
                            var smallImage = document.getElementById('smallImage');
                            var imagPlce = document.getElementById('imgPlceHoldr');
                                imagPlce.style.display = 'none';

                            // Unhide image elements
                            smallImage.style.display = 'block';
                            smallImage.src = image;

                            //Store localy
                            dataService.saveStoreIMG(
                                $scope.layout.store.id,
                                image
                            )
                            .success(function(data){
                                $ionicPopup.show({
                                    title    : '<div class="popup-pmi-title">Saving ... !</div>',
                                    template : '<div class="popup-subtitle">Saving Image Information</div>',
                                    buttons  : [
                                        {
                                            text: 'OK',
                                            onTap: function () {
                                                //$state.go('app.storeDetails');
                                            }
                                        }
                                    ]
                                });
                            })
                            .error(function(error){
                                alert("Could not save image to Local storage");
                            });

                             // Retrieve the image from Local
                            dataService.getImageStore($scope.layout.store.id)
                            .success(function(imageData){
                                //console.log(imageData); return false;

                                for(var n = 0; n < imageData.rows.length; n++){
                                    var data = JSON.parse(imageData.rows.item(n).store_content);

                                    if(data.STOREID == $scope.layout.store.id){
                                       var img = data.IMAGE;
                                    }
                                }

                                //alert('you made it here');
                                //alert(img);
                                var smallImage = document.getElementById('smallImage');
                                var imagPlce = document.getElementById('imgPlceHoldr');
                                imagPlce.style.display = 'none';

                            // Unhide image elements
                            smallImage.style.display = 'block';
                            smallImage.style.width = '140px';
                            smallImage.style.height = '100px';
                            smallImage.src = img;
                                
                            })
                            .error(function(error){
                                alert("Could not get image from Localstorage");
                            });

                             dataService.syncStoreIMAGE()
                            .success( function(res){
                                console.log(res);
                            })
                            .error(function(error){
                                //alert("error at StoresCtrl");
                                $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">IMAGE SYNC</div>',
                                        template:'<div class="popup-subtitle">Data Error</div>',
                                        buttons: [
                                            {
                                                text: 'OK',
                                                type: 'button-pmi',
                                                onTap: function () {
                                                }
                                            }
                                        ]
                                    });
                            });
                            

                        }

                        function onFail(message) {
                            LoaderService.hideLoading();
                            //alert('Failed because: ' + message);
                            $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Image Captre Failed</div>',
                                    template:'<div class="popup-subtitle"> '+ message +' </div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            onTap: function () {
                                                //$state.go('login');
                                            }
                                        }
                                    ]
                                });
                        }
                    };
                   
                    // Retrieve the image from Local
                    dataService.getImageFurniture($scope.layout.store.id)
                    .success(function(imageData){
                        //alert('you made it here');

                        for(var n = 0; n < imageData.rows.length; n++){
                            var data = JSON.parse(imageData.rows.item(n).store_content);

                            if(data.STOREID == $scope.layout.store.id){
                               var img = data.IMAGE;
                            }
                        }

                        //alert('you made it here');
                        //alert(img);
                        var smallImage = document.getElementById('FsmallImage');
                        var imagPlce = document.getElementById('FimgPlceHoldr');
                        imagPlce.style.display = 'none';

                        // Unhide image elements
                        smallImage.style.display = 'block';
                        smallImage.style.width = '140px';
                        smallImage.style.height = '100px';
                        smallImage.src = img;
                        
                    })
                    .error(function(error){
                        alert("Could not get image from Localstorage");
                    });

                     // Retrieve the image from Local
                    dataService.getImageStore($scope.layout.store.id)
                    .success(function(imageData){
                        //alert('you made it here');

                        for(var n = 0; n < imageData.rows.length; n++){
                            var data = JSON.parse(imageData.rows.item(n).store_content);

                            if(data.STOREID == $scope.layout.store.id){
                               var img = data.IMAGE;
                            }
                        }

                        //alert('you made it here');
                        //alert(img);
                        var smallImage = document.getElementById('smallImage');
                        var imagPlce = document.getElementById('imgPlceHoldr');
                        imagPlce.style.display = 'none';

                        // Unhide image elements
                        smallImage.style.display = 'block';
                        smallImage.style.width = '140px';
                        smallImage.style.height = '100px';
                        smallImage.src = img;
                        
                    })
                    .error(function(error){
                        alert("Could not get image from Localstorage");
                    });

                    // PAST STATS
                    var pastData = Array();
                    var worldData = '';
                    if($scope.layout.store.past_stats == '' || (Array.isArray($scope.layout.store.past_stats) && $scope.layout.store.past_stats.length < 1) || Object.keys($scope.layout.store.past_stats).length < 1){
                        pastData = ['0','0','0','0','0','0','0','0','0','0','0'];
                        worldData = ['0','0','0','0','0','0','0','0','0','0','0'];
                        $scope.emptyGraph = '0';
                    }else{
                        $scope.emptyGraph = '1';
                        worldData = $scope.layout.world;
                        angular.forEach($scope.layout.store.past_stats,function(yearData, year) {
                            angular.forEach(yearData,function(monthData, month) {
                                angular.forEach(monthData,function(QuestionData, Question) {
                                    var V1P1 = (QuestionData.V1A == 'yes') ? 1 : 0 ;
                                    var V2P1 = (QuestionData.V2A == 'yes') ? 1 : 0 ;
                                    var V3P1 = (QuestionData.V3A == 'yes') ? 1 : 0 ;
                                    var V4P1 = (QuestionData.V4A == 'yes') ? 1 : 0 ;
                                    if(pastData[Question - 1]){
                                        pastData[Question - 1] = parseInt(pastData[Question - 1]) +  V1P1 + V2P1 + V3P1 + V4P1;
                                    }else{
                                        pastData[Question - 1] = V1P1 + V2P1 + V3P1 + V4P1;
                                    }
                                });
                            });
                        });
                    }

                    // TOTAL POINTS
                    $scope.totalPoints = 0;
                    angular.forEach(pastData,function(value, key) {
                        $scope.totalPoints += parseInt(value);
                    });

                    // GRAPH
                    $scope.graph = {};
                    $scope.graph.labels = ['1','2','3','4','5','6','7','8','9','10','11','12','13'];
                    $scope.graph.data = [
                        pastData,
                        worldData
                    ];
                    $scope.graph.colours = [
                        {
                            fillColor: '#38538E',
                        },
                        {
                            fillColor: '#2B4273',
                        }
                    ];
                    $scope.graph.series = ['SA Score', 'World Wide Score'];
                    $scope.graph.options = {
                        scaleGridLineColor: "#fff",
                        scaleShowVerticalLines: false,
                        scaleFontFamily: "'source_sans_proregular', 'Helvetica', 'Arial', sans-serif",
                        tooltipFillColor: "#231f20",
                        tooltipFontFamily: "'source_sans_proregular', 'Helvetica', 'Arial', sans-serif",
                        tooltipTitleFontFamily: "'source_sans_proregular', 'Helvetica', 'Arial', sans-serif",
                        tooltipTitleFontStyle: "normal",
                        scaleFontColor: "#fff",
                        barValueSpacing: 10,
                        barShowStroke: false,
                        barDatasetSpacing: 0,
                        scaleLineColor: "#fff"
                    };
                });

                // CHECK PAST RESULTS
                $scope.pastResults = function(){
                    $state.go('app.pastResults');
                };

                // GO TO HOME
                $scope.goToHome = function(){
                    if($scope.layout.store.call_status == 'Démarrer')
                    {
                        LoaderService.showLoading('Starting Call');
                        dataService.start($scope.layout.store.id).success(function (result) {
                            $scope.layout.store.current_stats = result['CURRENT_STATS'];
                            $scope.layout.store.call_status = result['CALL_STATUS'];
                            $scope.layout.store.position = result['POSITION'];
                            LoaderService.hideLoading();
                            $state.go('app.storeHome');
                        });
                    }else{
                        LoaderService.hideLoading();
                        $state.go('app.storeHome');
                    }
                };
            });
        })

        // PAST RESULTS :: pastResultsCtrl
        .controller('pastResultsCtrl', function($ionicPlatform, $scope, $state, $filter, $http){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.store = $scope.layout.store;
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Past Results',link:''}
                    ];

                    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    $scope.currDate = $filter('date')(new Date(), 'yyyy', '+0200');
                                        
                    // CHECK CURRENT MONTH
                    var dateFunc = new Date();
                    $scope.currentmonth = dateFunc.getMonth()+1;
                    $scope.currentmonthName = monthNames[dateFunc.getMonth()];
                    $scope.currentyear = dateFunc.getFullYear();

                    $scope.data = Array();
                    $scope.data['TOTALS'] = {};
                    $scope.data['QUESTIONS'] = {};
                    if($scope.layout.store.past_stats[$scope.currentyear]){
                        for (var i = 1; i < 14; i++) {
                            for (var k = 1; k < 13; k++) {
                                if(!$scope.data['QUESTIONS'][i])
                                    $scope.data['QUESTIONS'][i] = {};
                                if(!$scope.data['QUESTIONS'][i][k])
                                    $scope.data['QUESTIONS'][i][k] = 'N/A';
                                if(!$scope.data['TOTALS'][k])
                                    $scope.data['TOTALS'][k] = 0;

                                if($scope.layout.store.past_stats[$scope.currentyear][k]){
                                    if($scope.layout.store.past_stats[$scope.currentyear][k][i]){
                                        var V1P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V1A == 'yes') ? 1 : 0 ;
                                        var V2P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V2A == 'yes') ? 1 : 0 ;
                                        var V3P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V3A == 'yes') ? 1 : 0 ;
                                        var V4P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V4A == 'yes') ? 1 : 0 ;
                                        $scope.data['QUESTIONS'][i][k] = V1P2 + V2P2 + V3P2 + V4P2;
                                        $scope.data['TOTALS'][k] += parseInt($scope.data['QUESTIONS'][i][k]);
                                    }
                                }else{
                                    $scope.data['TOTALS'][k] = 'N/A';
                                }
                            }
                        }
                    }else{
                        for (var i = 1; i < 14; i++) {
                            for (var k = 1; k < 13; k++) {
                                if(!$scope.data['QUESTIONS'][i])
                                    $scope.data['QUESTIONS'][i] = {};
                                if(!$scope.data['QUESTIONS'][i][k])
                                    $scope.data['QUESTIONS'][i][k] = 'N/A';
                                if(!$scope.data['TOTALS'][k])
                                    $scope.data['TOTALS'][k] = 'N/A';
                            }
                        }
                    }

                    for (var i = 1; i < Object.keys($scope.data['TOTALS']).length; i++) {
                        if($scope.data['TOTALS'][i] != 'N/A')
                        {
                            if($scope.data['TOTALS'][i + 1] && $scope.data['TOTALS'][i + 1] != 'N/A')
                            {
                                $scope.data['TOTALS'][i + 1] = parseInt($scope.data['TOTALS'][i + 1]) + parseInt($scope.data['TOTALS'][i]);
                            }
                        }
                    }
                });

                // BACK TO DETAILS
                $scope.backToDetails = function(){
                    $state.go('app.storeDetails');
                };
            });
        })

        // STORE HOME :: storeHomeCtrl
        .controller('storeHomeCtrl', function($ionicPlatform, $ionicPopup, $scope, $state, LoaderService, dataService, $cordovaGeolocation, $ionicModal, $rootScope, $log){

            $ionicPlatform.ready(function() {

                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.store = $scope.layout.store;

                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',            link:'app.stores'},
                        {title:$scope.layout.store.name,    link:'app.storeDetails'},
                        {title:'Home',                      link:''}
                    ];
                });

                /*$scope.showCollectInfo = true;
                dataService.collectInfo()
                .success( function(res){
                    $scope.showCollectInfo = false;
                })
                .error(function(error){
                    alert("error");
                })*/
                

                // GRAB LOCATION
                $scope.getLocation = function(){
                    var posOptions = {timeout: 10000, enableHighAccuracy: false};
                    $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position){
                        $scope.layout.store.position.LAT = position.coords.latitude;
                        $scope.layout.store.position.LONG = position.coords.longitude;

                        dataService.updatePos($scope.layout.store.position.LAT , $scope.layout.store.position.LONG,$scope.layout.store.id).success(function (result) {
                            if(checknet == 'online'){
                                dataService.sync().success(function (result) {
                                    LoaderService.hideLoading();
                                    if(result != 'active'){
                                        ipCookie.remove('LOGIN');
                                        $ionicPopup.show({
                                            title: '<div class="popup-pmi-title">Update Questions Synching</div>',
                                            template:'<div class="popup-subtitle">Incorrect login details</div>',
                                            buttons: [
                                                {
                                                    text: 'To login page',
                                                    onTap: function () {
                                                        $state.go('login');
                                                    }
                                                }
                                            ]
                                        });
                                    }
                                }).error(function(){
                                    LoaderService.hideLoading();
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Update questions Sync Failed</div>',
                                        template:'<div class="popup-subtitle">Data Error</div>',
                                        buttons: [
                                            {
                                                text: 'To login page',
                                                onTap: function () {
                                                    $state.go('login');
                                                }
                                            }
                                        ]
                                    });
                                });
                            }else{
                                LoaderService.hideLoading();
                            }
                        }).error(function(){
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Update Location Failed</div>',
                                template:'<div class="popup-subtitle">Data Error</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        });
                    }, function(error){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Error</div>',
                            template:'<div class="popup-subtitle">Location Cannot be captured.</div>',
                            buttons: [
                                {
                                    text: 'Ok'
                                }
                            ]
                        });
                    });
                };

                // BACK TO DETAILS
                $scope.backToDetails = function(){
                    $state.go('app.storeDetails');
                };
                
                $scope.store = [];
                $scope.colectDATA = function(store){
                    //console.log(store);
                    //$rootScope.$broadcast('STORE', $scope.layout.store);
                    $state.go('app.data-collection', { obj:store });
                };

                // REDIRECT
                $scope.goTo = function(A) {
                    LoaderService.showLoading('Loading...');
                    if($scope.layout.store.call_status == "Démarrer") {
                        LoaderService.showLoading('Starting Call');
                        dataService.start($scope.layout.store.id).success(function (result) {
                            $scope.layout.store.current_stats = result['CURRENT_STATS'];
                            $scope.layout.store.call_status = result['CALL_STATUS'];
                            $scope.layout.store.position = result['POSITION'];
                            LoaderService.hideLoading();
                            $state.go(A);
                        });
                    }else{
                        $state.go(A);
                    }
                };
                
            });
        })

        
        // DATA COLLECTION :: dataCollectionCtrl
        .controller('dataCollectionCtrl', function($ionicPlatform, $ionicPopup, $scope, $state, LoaderService, dataService, $cordovaGeolocation, $ionicModal, $filter, $controller, $stateParams){
            $ionicPlatform.ready( function () {

                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.store = $scope.layout.store;
                    $scope.storeNAME = $scope.layout.store.name; 
                    $scope.storeID = $scope.layout.store.id;

                    $scope.layout.breadcrumbs = [

                        { title : 'Back to Stores',            link : 'app.stores'},
                        { title : $scope.layout.store.name,    link : 'app.storeDetails'},
                        { title : 'Data Collection',           link : 'app.data-collection'},
                        { title : 'Home',                      link : ''}
                    ];
                });

                $scope.$on('$ionicView.enter', function(){
                    (function(){
                        
                        dataService.storeEmployees($scope.layout.store.id)
                        .success( function (res) {
                            angular.forEach(res, function (value, key) {
                                if (key == $scope.layout.store.id) {

                                    // MANAGERS
                                    if ( value.EMPLOYEES[0].area_manager_1_name != undefined ) {
                                        $scope.data.manager = { manager_01      : value.EMPLOYEES[0].area_manager_1_name };
                                    }
                                    if ( value.EMPLOYEES[0].area_manager_1_tel != undefined ) {
                                        $scope.data.manager = { manager_01_phone    : value.EMPLOYEES[0].area_manager_1_tel };
                                    }
                                    if ( value.EMPLOYEES[0].area_manager_2_name != undefined ) {
                                        $scope.data.manager = { manager_02      : value.EMPLOYEES[0].area_manager_2_name };
                                    }
                                    if ( value.EMPLOYEES[0].area_manager_2_tel != undefined) {
                                        $scope.data.manager = { manager_02_phone    : value.EMPLOYEES[0].area_manager_2_tel };
                                    }
                                    if ( value.EMPLOYEES[0].pos_address != undefined) {
                                        $scope.data.manager = { address         : value.EMPLOYEES[0].pos_address };
                                    }
                                    if(value.EMPLOYEES[0].cashier_1_name != undefined){
                                        $scope.data.cashier_01 = { name     : value.EMPLOYEES[0].cashier_1_name };
                                    }

                                    // CASHIER 01
                                    if ( value.EMPLOYEES[0].cashier_1_tel  != undefined ) {
                                        $scope.data.cashier_01 = { phone    : value.EMPLOYEES[0].cashier_1_tel };
                                    }
                                    if ( value.EMPLOYEES[0].cashier_1_mail != undefined) {
                                        $scope.data.cashier_01 = { emil     : value.EMPLOYEES[0].cashier_1_mail};
                                    }
                                    
                                    if ( value.EMPLOYEES[0].cashier_1_date_of_birth != undefined ) {
                                        $scope.data.cashier_01 = { birthday : value.EMPLOYEES[0].cashier_1_date_of_birth};
                                    }
                                    

                                    //CASHIER 02
                                    if ( value.EMPLOYEES[0].cashier_2_name != undefined ) {
                                        $scope.data.cashier_02 = { name     : value.EMPLOYEES[0].cashier_2_name };
                                    }
                                    
                                    if ( value.EMPLOYEES[0].cashier_2_tel != undefined ) {
                                        $scope.data.cashier_02 = { phone    : value.EMPLOYEES[0].cashier_2_tel};
                                    }
                                    
                                    if ( value.EMPLOYEES[0].cashier_2_mail != undefined ) {
                                        $scope.data.cashier_02 = { emil     : value.EMPLOYEES[0].cashier_2_mail};
                                    }
                                    
                                    if ( value.EMPLOYEES[0].cashier_2_date_of_birth != undefined ) {
                                        $scope.data.cashier_02 = { birthday : value.EMPLOYEES[0].cashier_2_date_of_birth};
                                    }
                                    //

                                    // CASHIER 03
                                    if ( value.EMPLOYEES[0].cashier_3_name != undefined ) {
                                        $scope.data.cashier_03 = { name     : value.EMPLOYEES[0].cashier_3_name };
                                    }
                                    //
                                    
                                    if ( value.EMPLOYEES[0].cashier_3_tel != undefined ) {
                                        $scope.data.cashier_03 = { phone    : value.EMPLOYEES[0].cashier_3_tel};
                                    }
                                    
                                    if ( value.EMPLOYEES[0].cashier_3_mail != undefined ) {
                                        $scope.data.cashier_03 = { emil     : value.EMPLOYEES[0].cashier_3_mail};
                                    }
                                    
                                    if ( value.EMPLOYEES[0].cashier_3_date_of_birth != undefined) {
                                        $scope.data.cashier_03 = { birthday : value.EMPLOYEES[0].cashier_3_date_of_birth};
                                    }
                                }
                            });
                        })
                        .error( function (error) {
                            console.log(error);
                        });
                    }());
                });
           
                
                $scope.storeNAME = $scope.layout.store.name;
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                $scope.currDate = $filter('date')(new Date(), 'yyyy', '+0200');
                $scope.currentmonthName = monthNames[new Date().getMonth()];

                


                $scope.data = [];
                $scope.createContact = function(data) {
                    //LoaderService.showLoading('Saving data...');
                    dataService.collectInfo(
                        /* Sore ID */
                        $scope.layout.store.id,
                        
                        /* Sore ID */
                        $scope.data.manager,

                        /* Cashier 01 */ 
                        $scope.data.cashier_01,
                        
                        /* Cashier 02 */
                        $scope.data.cashier_02,
                        
                        /* Cashier 03 */
                        $scope.data.cashier_03)
                    .success( function (result) {
                        //
                        $scope.data = [];
                        LoaderService.hideLoading();
                        $ionicPopup.show({
                            title    : '<div class="popup-pmi-title">Saving ... !</div>',
                            template : '<div class="popup-subtitle">Saving Information</div>',
                            buttons  : [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                        $state.go('app.storeDetails');
                                    }
                                }
                            ]
                        });
                    })
                    .error ( function (error) {
                        LoaderService.hideLoading();
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Saving Info Failed</div>',
                            template:'<div class="popup-subtitle">Please try again later</div>',
                            buttons: [
                                {
                                    text: 'To login page',
                                    onTap: function () {
                                        $state.go('app.storeDetails');
                                    }
                                }
                            ]
                        });
                    })
                    .finally( function (fin) {});
                };

                $scope.$on('$ionicView.leave', function(){
                    $scope.data = [];
                });

                $scope.backToDetails = function() {
                    $state.go('app.storeDetails');
                };

            });        
        })


        // STORE QUESTIONS :: storeQuestionsCtrl
        .controller('storeQuestionsCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService, $ionicPopup, ipCookie, $filter){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {

                    LoaderService.showLoading('Loading...');
                    $scope.store = $scope.layout.store;
                    $scope.layout.breadcrumbs = [
                        { title : 'Back to Stores',           link : 'app.stores'},
                        { title : $scope.layout.store.name,   link : 'app.storeDetails'},
                        { title : 'Home',                     link : 'app.storeHome'},
                        { title : 'Questions',                link : ''}
                    ];

                    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    $scope.currDate = $filter('date')(new Date(), 'yyyy', '+0200');

                    var dateFunc = new Date();
                    $scope.currentmonthName = monthNames[dateFunc.getMonth()];

                    dataService.store().success(function (result) {
                        var serviceStats = JSON.parse(result);
                        $scope.page = {};
                        $scope.page.currentStats = serviceStats[$scope.layout.store.id]['CURRENT_STATS'] ;
                        $scope.commentvalue = '';
                        $scope.visit_val = '';
                        $scope.question_val = '';
                        $scope.visitImage = '';
                        $scope.visitComment = '';
                        $scope.disabled = false;

                        $scope.current_image_store = [];

                        $scope.visit = '1';
                        $scope.visitAnswer = '';
                        $scope.imgSRC = 'img/placeholder.png';

                        angular.forEach($scope.page.currentStats,function(value, key) {
                            $scope.currentYear = key;
                            $scope.layout.year = key;
                        });

                        angular.forEach($scope.page.currentStats[$scope.currentYear],function(value, key) {
                            $scope.currentMonth = key;
                            $scope.layout.month = key;
                        });

                        $scope.points = 0;
                        $scope.update = 0;
                        $scope.submitMonth = null;
                        $scope.submitYear = null;
                        $scope.submision_text = 'Update';
                        $scope.question_number = Object.keys($scope.page.currentStats[$scope.currentYear][$scope.currentMonth]).length;

                        // CHECK DATA
                        checkData();
                        checkStatus();
                        LoaderService.hideLoading();
                    });
                });

                // BACK TO DETAILS
                $scope.backToHome = function(){
                    $state.go('app.storeHome');
                };

                $scope.check = function(Visit, Question){
                    $scope.update = 1;
                    if(Question == '13')
                        $scope.submision_text = 'Submit Visit';
                    else
                        $scope.submision_text = 'Update';

                    var currentdate = new Date();
                    var datetime =  currentdate.getFullYear() +"-"
                        + (currentdate.getMonth()+1) +"-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][Question]['V'+Visit+'DT'] = datetime;
                    checkData();
                }

                $scope.updateQ = function(){
                    $scope.update = 0;
                    LoaderService.showLoading('Updating Data');
                    checkStatus();
                    dataService.updateQ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth], 
                        $scope.layout.store.id,$scope.currentYear,$scope.currentMonth)
                    .success( function (result) {
                        if (result == 1) {
                            angular.forEach($scope.page.currentStats[$scope.currentYear][$scope.currentMonth],function(value, key) {
                                value.V1A = ''; value.V1A = ''; value.V1A = '';
                                value.V2A = ''; value.V2A = ''; value.V2A = '';
                                value.V3A = ''; value.V3A = ''; value.V3A = '';
                                value.V4A = ''; value.V4A = ''; value.V4A = '';
                            });
                            $state.go('app.storeHome');
                        }
                        //console.log("Update question || Fires for the same Store on same month, I presume");
                        //console.log(result); return false;
                        if(checknet == 'online'){
                            dataService.sync()
                            .success(function (result) {
                                LoaderService.hideLoading();
                                if(result != 'active'){
                                    ipCookie.remove('LOGIN');
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Update Questions Synching</div>',
                                        template:'<div class="popup-subtitle">Incorrect login details</div>',
                                        buttons: [
                                            {
                                                text: 'To login page',
                                                onTap: function () {
                                                    $state.go('login');
                                                }
                                            }
                                        ]
                                    });
                                }
                            })
                            .error(function(){
                                LoaderService.hideLoading();
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Update questions Sync Failed</div>',
                                    template:'<div class="popup-subtitle">Data Error</div>',
                                    buttons: [
                                        {
                                            text: 'To login page',
                                            onTap: function () {
                                                $state.go('login');
                                            }
                                        }
                                    ]
                                });
                            });
                        }else{
                            LoaderService.hideLoading();
                        }
                    })
                    .error(function(){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Update Questions Failed</div>',
                            template:'<div class="popup-subtitle">Data Error</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    });
                }

                $scope.submit = function(){
                    LoaderService.showLoading('Submiting Data');
                    dataService.complete($scope.layout.store.id,$scope.currentYear,$scope.currentMonth).success(function (result) {
                        if(checknet == 'online'){
                            LoaderService.showLoading('Loading Data');
                            dataService.sync().success(function (result2) {
                                LoaderService.hideLoading();
                                if(result2 != 'active'){
                                    ipCookie.remove('LOGIN');
                                    $ionicPopup.show({
                                        title: '<div class="popup-pmi-title">Submit Questions Synching</div>',
                                        template:'<div class="popup-subtitle">Incorrect login details</div>',
                                        buttons: [
                                            {
                                                text: 'To login page',
                                                onTap: function () {
                                                    $state.go('login');
                                                }
                                            }
                                        ]
                                    });
                                }else{
                                    dataService.store().success(function (result) {
                                        var returnSubmitteddata = JSON.parse(result);

                                        $scope.layout.store.current_stats = returnSubmitteddata[$scope.layout.store.id]['CURRENT_STATS'];
                                        $scope.layout.store.past_stats = returnSubmitteddata[$scope.layout.store.id]['PAST_STATS'];
                                        $scope.layout.store.call_status = returnSubmitteddata[$scope.layout.store.id]['CALL_STATUS'];
                                        $scope.layout.world = dataService.world();

                                        LoaderService.hideLoading();
                                        $scope.layout.call_completed = true;
                                        $state.go('app.storeSummary');
                                    });
                                }
                            }).error(function(){
                                LoaderService.hideLoading();
                                $ionicPopup.show({
                                    title: '<div class="popup-pmi-title">Submit questions Sync Failed</div>',
                                    template:'<div class="popup-subtitle">Data Error</div>',
                                    buttons: [
                                        {
                                            text: 'OK',
                                            onTap: function () {
                                            }
                                        }
                                    ]
                                });
                            });
                        }else{
                            dataService.store().success(function (result) {
                                var returnSubmitteddata = JSON.parse(result);

                                $scope.layout.store.current_stats = returnSubmitteddata[$scope.layout.store.id]['CURRENT_STATS'];
                                $scope.layout.store.past_stats = returnSubmitteddata[$scope.layout.store.id]['PAST_STATS'];
                                $scope.layout.store.call_status = returnSubmitteddata[$scope.layout.store.id]['CALL_STATUS'];
                                $scope.layout.world = dataService.world();

                                LoaderService.hideLoading();
                                $state.go('app.storeSummary');
                            });
                        }
                    }).error(function(){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Submiting data Failed</div>',
                            template:'<div class="popup-subtitle">Data Error</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    });
                }

                function checkData(){
                    $scope.points = 0;
                    angular.forEach($scope.page.currentStats[$scope.currentYear][$scope.currentMonth],function(value, key) {
                        var V1P3 = (value.V1A == 'yes') ? 1 : 0 ;
                        var V2P3 = (value.V2A == 'yes') ? 1 : 0 ;
                        var V3P3 = (value.V3A == 'yes') ? 1 : 0 ;
                        var V4P3 = (value.V4A == 'yes') ? 1 : 0 ;
                        $scope.points += V1P3 + V2P3 + V3P3 + V4P3;
                    });
                }

                function checkStatus(){
                    var visit1Check = true;
                    var visit2Check = true;
                    var visit3Check = true;
                    var visit4Check = true;
                    angular.forEach($scope.page.currentStats[$scope.currentYear][$scope.currentMonth],function(value, key) {
                        if(value.V1A != 'yes' && value.V1A != 'no' && value.V1A != 'na'){
                            visit1Check = false;
                        }
                        if(value.V2A != 'yes' && value.V2A != 'no' && value.V2A != 'na'){
                            visit2Check = false;
                        }
                        if(value.V3A != 'yes' && value.V3A != 'no' && value.V3A != 'na'){
                            visit3Check = false;
                        }
                        if(value.V4A != 'yes' && value.V4A != 'no' && value.V4A != 'na'){
                            visit4Check = false;
                        }
                    });

                    if(!visit1Check){
                        $scope.visit = '1';
                        $scope.visitAnswer = 'V1A';
                        $scope.visitImage = 'V1I';
                        $scope.visitComment = 'V1C';
                    }else if(!visit2Check){
                        $scope.visit = '2';
                        $scope.visitAnswer = 'V2A';
                        $scope.visitImage = 'V2I';
                        $scope.visitComment = 'V2C';
                    }else if(!visit3Check){
                        $scope.visit = '3';
                        $scope.visitAnswer = 'V3A';
                        $scope.visitImage = 'V3I';
                        $scope.visitComment = 'V3C';
                    }else if(!visit4Check){
                        $scope.visit = '4';
                        $scope.visitAnswer = 'V4A';
                        $scope.visitImage = 'V4I';
                        $scope.visitComment = 'V4C';
                    }else{
                        $scope.visit = '4';
                        $scope.visitAnswer = 'V4A';
                        $scope.visitImage = 'V4I';
                        $scope.visitComment = 'V4C';
                        $scope.disabled = true;
                        $scope.submitMonth = 1;
                    }
                }

                $scope.commentModal = function(stats, question, visit){
                    $scope.comment_modal = stats;

                    if(stats == 'true')
                    {
                        $scope.visit_val = visit;
                        $scope.question_val = question;
                        $scope.commentvalue = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'];
                    }else{
                        $scope.commentvalue = '';
                    }
                }

                $scope.submitCommentModal = function(comment ,question, visit){
                    $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] = comment;

                    // $scope.updateQ();
                    $scope.comment_modal = 'false';
                }

                $scope.takePicture = function(question, visit){
                    LoaderService.showLoading('Taking Picture...');
                    var currentdate = new Date();
                    var datetime =  currentdate.getFullYear() +"-"
                        + (currentdate.getMonth()+1) +"-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    navigator.camera.getPicture(onSuccess, onFail, { quality: 100,targetWidth: 480,targetHeight: 480,
                        destinationType: Camera.DestinationType.DATA_URL
                    });

                    function onSuccess(imageData) {
                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] == '' || $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] == null)
                            $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] = "data:image/jpeg;base64," + imageData;
                        else
                            $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] + "|data:image/jpeg;base64," + imageData;
                        LoaderService.hideLoading();
                        // $scope.submitImageModal(question, visit);
                        $scope.imageModal('true',question, visit);
                    }

                    function onFail(message) {
                        LoaderService.hideLoading();
                        alert('Failed because: ' + message);
                    }
                }

                $scope.imageModal = function(stats, question, visit){
                    $scope.current_image_store = [];
                    $scope.image_modal = stats;
                    $scope.visit_val = visit;
                    $scope.question_val = question;
                    if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != null){
                        $scope.imgSRC = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'];
                        $scope.current_image_store = $scope.imgSRC.split('|');
                    }else{
                        $scope.imgSRC = 'img/placeholder.png';
                    }
                }

                $scope.submitImageModal = function(question, visit){
                    $scope.image_modal = 'false';
                }
            });
        })

        // STORE SUMMARY ::storeSummaryCtrl
        .controller('storeSummaryCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService, $ionicPopup){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    LoaderService.showLoading('Loading...');
                    $scope.store = $scope.layout.store;
                    $scope.call_completed = $scope.layout.call_completed;
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Home',link:'app.storeHome'},
                        {title:'Summary',link:''}
                    ];

                    dataService.store().success(function (result) {
                        var resultser = JSON.parse(result);
                        var serviceStats = resultser[$scope.layout.store.id];
                        $scope.currentMonth = 0;
                        $scope.currentYear = 0;
                        $scope.points = 0;
                        $scope.VisistPoints = [0, 0, 0, 0];
                        $scope.current_image_store = [];
                        $scope.page = {};


                        if($scope.layout.store.call_status == 'Continuer'){
                            $scope.page.currentStats = serviceStats['CURRENT_STATS'] ;
                            angular.forEach($scope.page.currentStats,function(value, key) {
                                $scope.currentYear = key;
                            });
                            angular.forEach($scope.page.currentStats[$scope.currentYear],function(value, key) {
                                $scope.currentMonth = key;
                            });
                        }else if ($scope.layout.store.call_status == 'Terminé'){
                            var currentdate = new Date();
                            var month = currentdate.getMonth()+1;
                            var year = currentdate.getFullYear();
                            $scope.page.currentStats = serviceStats['PAST_STATS'] ;
                            $scope.currentMonth  = month;
                            $scope.currentYear  = year;
                        }else{
                            $scope.page.currentStats = serviceStats['PAST_STATS'] ;
                            $scope.currentMonth  = $scope.layout.month;
                            $scope.currentYear = $scope.layout.year;
                        }

                        $scope.DV1 = 1;$scope.DV2 = 1;$scope.DV3 = 1;$scope.DV4 = 1;

                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth])
                            $scope.question_number = Object.keys($scope.page.currentStats[$scope.currentYear][$scope.currentMonth]).length;

                        for (var i = 1; i < $scope.question_number + 1; i++) {
                            if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth] && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i]){
                                var V1P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V1A == 'yes') ? 1 : 0 ;
                                var V2P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V2A == 'yes') ? 1 : 0 ;
                                var V3P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V3A == 'yes') ? 1 : 0 ;
                                var V4P4 = ($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i].V4A == 'yes') ? 1 : 0 ;
                                $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][i]['POINTS'] = V1P4 + V2P4 + V3P4 + V4P4;
                                $scope.points += V1P4 + V2P4 + V3P4 + V4P4;
                                $scope.VisistPoints[0] += V1P4;
                                $scope.VisistPoints[1] += V2P4;
                                $scope.VisistPoints[2] += V3P4;
                                $scope.VisistPoints[3] += V4P4;
                            }
                        };

                        LoaderService.hideLoading();
                    });
                });

                // BACK TO DETAILS
                $scope.backToHome = function(){
                    $state.go('app.storeHome');
                }

                $scope.goToQ = function(){
                    $state.go('app.storeQuestions');
                }

                $scope.commentModal = function(stats, question, visit){
                    if(stats == 'true')
                    {
                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] !='' && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'] != null){
                            $scope.comment_modal = stats;


                            $scope.visit_val = visit;
                            $scope.question_val = question;
                            $scope.commentvalue = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'C'];
                        }else{
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Warning</div>',
                                template:'<div class="popup-subtitle">No Note made.</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        }
                    }else{
                        $scope.comment_modal = '';
                    }
                }

                $scope.imageModal = function(stats, question, visit){
                    if(stats == 'true')
                    {
                        $scope.current_image_store = [];
                        if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != '' && $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != null){
                            $scope.image_modal = stats;
                            if($scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'] != null){
                                $scope.imgSRC = $scope.page.currentStats[$scope.currentYear][$scope.currentMonth][question]['V'+visit+'I'];
                                $scope.current_image_store = $scope.imgSRC.split('|');
                            }else{
                                $scope.imgSRC = 'img/placeholder.png';
                            }
                        }else{
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Warning</div>',
                                template:'<div class="popup-subtitle">No Image taken.</div>',
                                buttons: [
                                    {
                                        text: 'OK',
                                        onTap: function () {
                                        }
                                    }
                                ]
                            });
                        }
                    }else{
                        $scope.image_modal = '';
                    }
                }
            });
        })

        // COMPETITOR :: competitorCtrl
        .controller('competitorCtrl', function($ionicPlatform, $scope, $state, dataService, LoaderService, $ionicPopup, $ionicModal){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    LoaderService.showLoading('Loading...');
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Home',link:'app.storeHome'},
                        {title:'Competitors',link:''}
                    ];

                    $scope.change = false;
                    $scope.notemodal = {};
                    $scope.notemodal.comment_value = '';
                    $scope.imagemodal = {};
                    $scope.competitor_image_store = {};

                    $scope.store = $scope.layout.store;
                    $scope.competitors = $scope.layout.competitor;
                    $scope.competitor_values = {};
                    $scope.competitor_posting_values = {};

                    angular.forEach($scope.competitors,function(value, key) {
                        $scope.competitor_values[key] = { images: '', comment : ''};
                        $scope.competitor_posting_values[key] = { images: '', comment : ''};
                        $scope.competitor_image_store[key] = [];
                    });

                    $scope.modals = {};
                    $scope.modals.current_competitor_id = '';

                    $ionicModal.fromTemplateUrl('imageModal.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        $scope.imageModal = modal;
                    });
                    $ionicModal.fromTemplateUrl('noteModal.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        $scope.noteModal = modal;
                    });

                    LoaderService.hideLoading();
                });

                // TAKE PICTURE
                $scope.takePicture = function(){
                    LoaderService.showLoading('Taking Picture...');
                    var currentdate = new Date();
                    var datetime =  currentdate.getFullYear() +"-"
                        + (currentdate.getMonth()+1) +"-"
                        + currentdate.getDate() + " "
                        + currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    navigator.camera.getPicture(onSuccess, onFail, { quality: 100,targetWidth: 480,targetHeight: 480,
                        destinationType: Camera.DestinationType.DATA_URL
                    });

                    function onSuccess(imageData) {
                        if($scope.competitor_values[$scope.modals.current_competitor_id]['images'] == '')
                            $scope.competitor_values[$scope.modals.current_competitor_id]['images'] = "data:image/jpeg;base64," + imageData;
                        else
                            $scope.competitor_values[$scope.modals.current_competitor_id]['images'] = $scope.competitor_values[$scope.modals.current_competitor_id]['images'] + "|data:image/jpeg;base64," + imageData;

                        $scope.competitor_image_store[$scope.modals.current_competitor_id] = $scope.competitor_values[$scope.modals.current_competitor_id]['images'].split('|');
                        LoaderService.hideLoading();
                    }

                    function onFail(message) {
                        LoaderService.hideLoading();
                        alert('Failed because: ' + message);
                    }
                }

                $scope.backToHome = function(){
                    $state.go('app.storeHome');
                }

                // IMAGE MODAL
                $scope.showImageModal = function(competitor_id){
                    $scope.modals.current_competitor_id = competitor_id;
                    $scope.imageModal.show();
                }

                // COMMENT MODAL
                $scope.showCommentModal = function(competitor_id){
                    $scope.modals.current_competitor_id = competitor_id;
                    $scope.noteModal.show();
                }

                // CLOSE MODAL
                $scope.close_modal = function(type){
                    var modal_type = ( type === 'images' )? 'imageModal' : 'noteModal' ;
                    $scope[modal_type].hide();
                }

                $scope.save_data = function(type, value){
                    if(type == 'comment'){
                        if(value && value != ''){
                            $scope.change = true;
                            $scope.competitor_posting_values[$scope.modals.current_competitor_id]['comment'] = value;
                            $scope.close_modal(type);
                        }
                    }else if (type == 'images'){
                        $scope.change = true;
                        $scope.competitor_posting_values[$scope.modals.current_competitor_id]['images'] = value;
                        $scope.close_modal(type);
                    }
                }

                // UPDATE COMPETITOR
                $scope.update_competitors = function(type){
                    $scope.close_modal(type);
                    LoaderService.showLoading('Saving data...');
                    dataService.updateComp($scope.competitor_posting_values, $scope.layout.store.id).success(function (result) {
                        LoaderService.hideLoading();
                        $scope.change = false;
                    }).error(function(){
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Update Competitors Faileds</div>',
                            template:'<div class="popup-subtitle">Data Error</div>',
                            buttons: [
                                {
                                    text: 'OK',
                                    onTap: function () {
                                    }
                                }
                            ]
                        });
                    });
                }
            });
        })

})();
