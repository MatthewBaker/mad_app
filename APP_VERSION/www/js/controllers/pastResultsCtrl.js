/**
 * Created by anele on 2017/07/13.
 */
/* =============================================================================
 *  pastResultsCtrl
 * =============================================================================
 */

(function() {
    'use strict';

    angular
        .module('starter')

        .controller('pastResultsCtrl', function($ionicPlatform, $scope, $state){
            $ionicPlatform.ready(function() {
                $scope.$on('$ionicView.beforeEnter', function() {
                    $scope.store = $scope.layout.store;
                    $scope.layout.breadcrumbs = [
                        {title:'Back to Stores',link:'app.stores'},
                        {title:$scope.layout.store.name,link:'app.storeDetails'},
                        {title:'Past Results',link:''}
                    ];

                    // CHECK CURRENT MONTH
                    var dateFunc = new Date();
                    $scope.currentmonth = dateFunc.getMonth()+1;
                    $scope.currentyear = dateFunc.getFullYear();

                    $scope.data = Array();
                    $scope.data['TOTALS'] = {};
                    $scope.data['QUESTIONS'] = {};
                    if($scope.layout.store.past_stats[$scope.currentyear]){
                        for (var i = 1; i < 14; i++) {
                            for (var k = 1; k < 13; k++) {
                                if(!$scope.data['QUESTIONS'][i])
                                    $scope.data['QUESTIONS'][i] = {};
                                if(!$scope.data['QUESTIONS'][i][k])
                                    $scope.data['QUESTIONS'][i][k] = 'N/A';
                                if(!$scope.data['TOTALS'][k])
                                    $scope.data['TOTALS'][k] = 0;

                                if($scope.layout.store.past_stats[$scope.currentyear][k]){
                                    if($scope.layout.store.past_stats[$scope.currentyear][k][i]){
                                        var V1P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V1A == 'yes') ? 1 : 0 ;
                                        var V2P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V2A == 'yes') ? 1 : 0 ;
                                        var V3P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V3A == 'yes') ? 1 : 0 ;
                                        var V4P2 = ($scope.layout.store.past_stats[$scope.currentyear][k][i].V4A == 'yes') ? 1 : 0 ;
                                        $scope.data['QUESTIONS'][i][k] = V1P2 + V2P2 + V3P2 + V4P2;
                                        $scope.data['TOTALS'][k] += parseInt($scope.data['QUESTIONS'][i][k]);
                                    }
                                }else{
                                    $scope.data['TOTALS'][k] = 'N/A';
                                }
                            }
                        }
                    }else{
                        for (var i = 1; i < 14; i++) {
                            for (var k = 1; k < 13; k++) {
                                if(!$scope.data['QUESTIONS'][i])
                                    $scope.data['QUESTIONS'][i] = {};
                                if(!$scope.data['QUESTIONS'][i][k])
                                    $scope.data['QUESTIONS'][i][k] = 'N/A';
                                if(!$scope.data['TOTALS'][k])
                                    $scope.data['TOTALS'][k] = 'N/A';
                            }
                        }
                    }

                    for (var i = 1; i < Object.keys($scope.data['TOTALS']).length; i++) {
                        if($scope.data['TOTALS'][i] != 'N/A')
                        {
                            if($scope.data['TOTALS'][i + 1] && $scope.data['TOTALS'][i + 1] != 'N/A')
                            {
                                $scope.data['TOTALS'][i + 1] = parseInt($scope.data['TOTALS'][i + 1]) + parseInt($scope.data['TOTALS'][i]);
                            }
                        }
                    }
                });

                // BACK TO DETAILS
                $scope.backToDetails = function(){
                    $state.go('app.storeDetails');
                }
            });
        })

})();