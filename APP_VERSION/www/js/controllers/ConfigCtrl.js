/**
 * Created by anele on 2017/07/13.
 */

/* =============================================================================
 *  ConfigCtrl
 * =============================================================================
 */

(function(){
    'use strict';

    angular
        .module('starter')

        .controller('ConfigCtrl', function($ionicPlatform, $scope, $state, $ionicPopup, ipCookie, dataService){
        $ionicPlatform.ready(function() {
            if(ipCookie('LOGIN')){
                $scope.user = ipCookie('LOGIN', undefined, {decode: function (value) { return value; }});
                $scope.feedback = 'Welcome ' + $scope.user.name + ", Loading Stores...";
                if(checknet == 'online'){
                    dataService.sync().success(function (result) {
                        if(result != 'active'){
                            ipCookie.remove('LOGIN');
                            $ionicPopup.show({
                                title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                                template:'<div class="popup-subtitle">Incorrect login details</div>',
                                buttons: [
                                    {
                                        text: 'To login page',
                                        onTap: function () {
                                            $state.go('login');
                                        }
                                    }
                                ]
                            });
                        }else{

                            $state.go('app.stores');
                        }
                    }).error(function(){
                        ipCookie.remove('LOGIN');
                        $ionicPopup.show({
                            title: '<div class="popup-pmi-title">Auto Login Failed</div>',
                            template:'<div class="popup-subtitle">Data Error</div>',
                            buttons: [
                                {
                                    text: 'To login page',
                                    onTap: function () {
                                        $state.go('login');
                                    }
                                }
                            ]
                        });
                    });
                }else{
                    $state.go('app.stores');
                }
            }else{
                $scope.feedback = "Loading App...";
                $state.go('login');
            }
        });
    })
})();